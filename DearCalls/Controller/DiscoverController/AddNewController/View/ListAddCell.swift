//
//  ListAddCell.swift
//  DearCalls
//
//  Created by dovietduy on 12/6/20.
//

import UIKit

class ListAddCell: UICollectionViewCell {

    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
