//
//  AddItemCell.swift
//  DearCalls
//
//  Created by dovietduy on 12/5/20.
//

import UIKit

class AddItemCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    
    var indexPath: IndexPath!
    var delegate: AddItemCellDelegate!
    var isSelect = true
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func didSelectBtnAdd(_ sender: Any) {
        delegate?.didSelectBtnAdd(cell: self)
    }
    
}
protocol AddItemCellDelegate: class{
    func didSelectBtnAdd(cell: AddItemCell)
}
