//
//  AddNewController.swift
//  DearCalls
//
//  Created by dovietduy on 12/5/20.
//

import UIKit
class AddNewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewBar: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewBack: UIView!
    var name: String? = ""
    var listData = JsonService.shared.readJsonFile()
    var index = 0
    var beforIndex = 0
    var isNewPlaylist = true
    var listAdd: [SoundModel] = []
    var playlist: PlaylistModel!
    var didSelectBtnAdd: ((_ playlist: PlaylistModel)->())?
    var didSelectBtnAddInLogVC: ((_ sound: SoundModel) -> Void)?
    var isPresentByLogVC = false
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            viewBar.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "ListAddCell", bundle: nil), forCellWithReuseIdentifier: "ListAddCell")
        
        tblView.dataSource = self
        tblView.delegate = self
        tblView.register(UINib(nibName: "AddItemCell", bundle: nil), forCellReuseIdentifier: "AddItemCell")
        listData.remove(at: 0)
        listData.remove(at: 0)
        if let name = name{
            lblTitle.text = "Add to \"\(name)\""
        }
        
    }
    @objc func didSelectBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
extension AddNewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListAddCell", for: indexPath) as! ListAddCell
        let item = listData[indexPath.row]
        cell.imgView.image = UIImage(named: item.image)
        cell.lblName.text = item.animalName
        if indexPath.row == index{
            cell.viewImage.dropShadow()
        } else {
            cell.viewImage.undropShadow()
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        index = indexPath.row
        collectionView.reloadData()
        tblView.reloadData()
    }
}

extension AddNewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 117 * scale, height: collectionView.bounds.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension AddNewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData[index].mSounds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddItemCell", for: indexPath) as! AddItemCell
        let item = listData[index].mSounds[indexPath.row]
        cell.lblTitle.text = item.Description
        cell.lblTime.text = item.timeFormat() + " (min:sec)"
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85 * scale
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! AddItemCell
        let soundAdd = listData[index].mSounds[cell.indexPath.row]
        listAdd.append(soundAdd)
        if isPresentByLogVC {
            cell.showToast(message: "\(soundAdd.Description) is added to your activity log")
            didSelectBtnAddInLogVC?(soundAdd)
        } else{
            playlist = PlaylistModel(name: name!, mSounds: listAdd)
            JsonService.shared.writeOjectToJson(playlist: playlist)
            cell.showToast(message: "\(soundAdd.Description) is added to playlist")
            didSelectBtnAdd?(playlist)
        }
    }
}

extension AddNewController: AddItemCellDelegate{
    func didSelectBtnAdd(cell: AddItemCell) {
        let soundAdd = listData[index].mSounds[cell.indexPath.row]
        listAdd.append(soundAdd)
        if isPresentByLogVC {
            didSelectBtnAddInLogVC?(soundAdd)
        } else{
            playlist = PlaylistModel(name: name!, mSounds: listAdd)
            JsonService.shared.writeOjectToJson(playlist: playlist)
            cell.showToast(message: "\(soundAdd.Description) is added to playlist")
            didSelectBtnAdd?(playlist)
        }
        
    }
}

