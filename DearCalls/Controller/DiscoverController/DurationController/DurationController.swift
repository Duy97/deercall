//
//  DurationController.swift
//  DearCalls
//
//  Created by dovietduy on 12/11/20.
//

import UIKit

class DurationController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewDelay: UIView!
    @IBOutlet weak var lblOFF1: UILabel!
    @IBOutlet weak var viewDelayDuration: UIView!
    @IBOutlet weak var lblOFF2: UILabel!
    @IBOutlet weak var viewRepeat: UIView!
    @IBOutlet weak var lblOFF3: UILabel!
    @IBOutlet weak var viewRepeatSetting: UIView!
    @IBOutlet weak var lblOFF4: UILabel!
    @IBOutlet weak var viewVolume: UIView!
    @IBOutlet weak var lbl100Volume: UILabel!
    @IBOutlet weak var imgBox1: UIImageView!
    @IBOutlet weak var imgBox2: UIImageView!
    @IBOutlet weak var viewBack: UIView!
    
    var isDelay = false
    var isRepeat = false
    var playlist: PlaylistModel!
    var index = 0
    var mSound: SoundModel!
    var didSendData: ((PlaylistModel) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        viewDelay.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewDelay(_:))))
        viewDelayDuration.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewDelayDuration(_:))))
        viewRepeat.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewRepeat(_:))))
        viewRepeatSetting.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewRepeatSetting(_:))))
        viewVolume.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewVolume(_:))))
        lblTitle.text = mSound.Description
        let setting = mSound.setting
        if setting.isDelay {
            viewDelayDuration.alpha = 1.0
            lblOFF1.text = "ON"
            lblOFF2.text = setting.delayDuration.format()
            imgBox1.image = #imageLiteral(resourceName: "ic_tick")
            isDelay = true
        }
        if setting.isRepeat {
            viewRepeatSetting.alpha = 1.0
            lblOFF3.text = "ON"
            lblOFF4.text = setting.repeatType + " - " + setting.repeatDuration + (setting.repeatType == "Finite" ? " times": " mins")
            imgBox2.image = #imageLiteral(resourceName: "ic_tick")
            isRepeat = true
        }
        lbl100Volume.text = setting.volume.description + "%"
    }
    @objc func didSelectViewDelay(_ sender: UITapGestureRecognizer){
        isDelay = !isDelay
        if isDelay == false {
            viewDelayDuration.alpha = 0.5
            lblOFF1.text = "OFF"
            lblOFF2.text = "OFF"
            imgBox1.image = #imageLiteral(resourceName: "ic_box")
            mSound.setting.isDelay = isDelay
        } else {
            viewDelayDuration.alpha = 1.0
            lblOFF1.text = "ON"
            lblOFF2.text = "OFF"
            imgBox1.image = #imageLiteral(resourceName: "ic_tick")
        }
    }
    @objc func didSelectViewDelayDuration(_ sender: UITapGestureRecognizer){
        if isDelay == true {
            let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpController") as! PopUpController
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            var strs: [Substring] = []
            if mSound.setting.delayDuration.contains(".") {
                strs = mSound.setting.delayDuration.split(separator: ".")
            }
            if mSound.setting.delayDuration.contains(",") {
                strs = mSound.setting.delayDuration.split(separator: ",")
            }
            if strs.count == 2{
                vc.strMinutes = String(strs[0])
                vc.strSeconds = String(strs[1])
            }
            present(vc, animated: true)
            vc.didSendData = {[weak self] strTime in
                if Double(strTime) == 0{
                    self?.viewDelayDuration.alpha = 0.5
                    self?.lblOFF1.text = "OFF"
                    self?.lblOFF2.text = "OFF"
                    self?.imgBox1.image = #imageLiteral(resourceName: "ic_box")
                    self?.isDelay = false
                    self?.mSound.setting.isDelay = false
                } else{
                    self?.lblOFF2.text = strTime.replacingOccurrences(of: ".", with: ":").replacingOccurrences(of: ",", with: ":") + " (min:sec)"
                    self?.mSound.setting.isDelay = true
                    self?.mSound.setting.delayDuration = strTime
                }
            }
        }
    }
    @objc func didSelectViewRepeat(_ sender: UITapGestureRecognizer){
        isRepeat = !isRepeat
        if isRepeat == false {
            viewRepeatSetting.alpha = 0.5
            lblOFF3.text = "OFF"
            lblOFF4.text = "OFF"
            imgBox2.image = #imageLiteral(resourceName: "ic_box")
            mSound.setting.isRepeat = isRepeat
        } else {
            viewRepeatSetting.alpha = 1.0
            lblOFF3.text = "ON"
            lblOFF4.text = "OFF"
            imgBox2.image = #imageLiteral(resourceName: "ic_tick")
        }
    }
    @objc func didSelectViewRepeatSetting(_ sender: UITapGestureRecognizer){
        if isRepeat == true {
            let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpRepeatController") as! PopUpRepeatController
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.strType = mSound.setting.repeatType
            var strs: [Substring] = []
            if mSound.setting.repeatDuration.contains(".") {
                strs = mSound.setting.repeatDuration.split(separator: ".")
            }
            if mSound.setting.repeatDuration.contains(",") {
                strs = mSound.setting.repeatDuration.split(separator: ",")
            }
            
            
            if strs.count == 0{
                vc.strMinutes = mSound.setting.repeatDuration
            } else if strs.count == 2 {
                vc.strMinutes = String(strs[0])
                vc.strSeconds = String(strs[1])
            }
            present(vc, animated: true)
            vc.didSendData = {[weak self] time, type, unit, icon in
                if Double(time) == 0.0 || Int(time) == 0 {
                    self?.viewRepeatSetting.alpha = 0.5
                    self?.lblOFF3.text = "OFF"
                    self?.lblOFF4.text = "OFF"
                    self?.imgBox2.image = #imageLiteral(resourceName: "ic_box")
                    self?.isRepeat = false
                    self?.mSound.setting.isRepeat = false
                    self?.mSound.setting.repeatType = ""
                    self?.mSound.setting.icon = icon
                } else{
                    self?.lblOFF4.text = type + " - " + time.replacingOccurrences(of: ".", with: ":").replacingOccurrences(of: ",", with: ":") + " " + unit
                    self?.mSound.setting.isRepeat = true
                    self?.mSound.setting.repeatType = type
                    self?.mSound.setting.repeatDuration = time
                    if type == "Finite"{
                        self?.lblOFF4.text = type + " - " + String(describing: Int(Double(time) ?? 0)) + " " + unit
                        self?.mSound.setting.repeatDuration = String(describing: Int(Double(time) ?? 0))
                    } else{
                        self?.lblOFF4.text = type + " - " + time + " " + unit
                        self?.mSound.setting.repeatDuration = time
                    }
                    self?.mSound.setting.icon = icon
                }
            }
        }
    }
    @objc func didSelectViewVolume(_ sender: UITapGestureRecognizer){
        let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVolumeController") as! PopUpVolumeController
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.volume = mSound.setting.volume
        present(vc, animated: true)
        vc.didSendData = {[weak self] volume in
            self?.lbl100Volume.text = volume.description + "%"
            self?.mSound.setting.volume = volume
        }
    }
    @objc func didSelectBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        playlist.mSounds[index] = mSound
        didSendData?(playlist)
    }
}
