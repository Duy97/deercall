//
//  EditPlaylistController.swift
//  DearCalls
//
//  Created by dovietduy on 12/10/20.
//

import UIKit

class EditPlaylistController: UIViewController {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewBack: UIView!
    var listAdd: [SoundModel] = []
    var playlist: PlaylistModel!
    var didSelectBtnDelete: ((_ playlist: PlaylistModel)->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        tblView.dataSource = self
        tblView.delegate = self
        tblView.register(UINib(nibName: "AddItemCell", bundle: nil), forCellReuseIdentifier: "AddItemCell")
    }
    @objc func didSelectBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
extension EditPlaylistController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlist.mSounds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddItemCell", for: indexPath) as! AddItemCell
        cell.btnAdd.setBackgroundImage(UIImage(named: "ic_delete"), for: .normal)
        cell.indexPath = indexPath
        let item = playlist.mSounds[indexPath.row]
        cell.lblTitle.text = item.Description
        cell.lblTime.text = item.timeFormat() + " (min:sec)"
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85 * scale
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! AddItemCell
        let alertController = UIAlertController(title: "Delete Call", message: "Delete \(cell.lblTitle.text!)? This cannot be undone", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "YES", style: .default, handler: { alert -> Void in
            self.playlist.mSounds.remove(at: cell.indexPath.row)
            JsonService.shared.writeOjectToJson(playlist: self.playlist)
            self.tblView.reloadData()
            self.didSelectBtnDelete?(self.playlist)
        })
        if let popoverController = alertController.popoverPresentationController { // fix for ipad
          popoverController.sourceView = self.view
          popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
          popoverController.permittedArrowDirections = []
        }
        let cancelAction = UIAlertAction(title: "NO", style: .cancel, handler: {(action : UIAlertAction!) -> Void in })
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
extension EditPlaylistController: AddItemCellDelegate{
    func didSelectBtnAdd(cell: AddItemCell) {
        let alertController = UIAlertController(title: "Delete Call", message: "Delete \(cell.lblTitle.text!)? This cannot be undone", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "YES", style: .default, handler: { alert -> Void in
            self.playlist.mSounds.remove(at: cell.indexPath.row)
            JsonService.shared.writeOjectToJson(playlist: self.playlist)
            self.tblView.reloadData()
            self.didSelectBtnDelete?(self.playlist)
        })
        if let popoverController = alertController.popoverPresentationController { // fix for ipad
          popoverController.sourceView = self.view
          popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
          popoverController.permittedArrowDirections = []
        }
        let cancelAction = UIAlertAction(title: "NO", style: .cancel, handler: {(action : UIAlertAction!) -> Void in })
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
extension EditPlaylistController{
    func openAlert(){
        
    }
}
