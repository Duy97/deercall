//
//  PlayListController.swift
//  DearCalls
//
//  Created by dovietduy on 12/5/20.
//

import UIKit

class PlayListController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewBar: UIView!
    @IBOutlet weak var viewBack: UIView!
    var listData = JsonService.shared.readJsonPlaylist()
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            viewBar.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "PlayListCell", bundle: nil), forCellWithReuseIdentifier: "PlayListCell")
        collectionView.register(UINib(nibName: "CreateCell", bundle: nil), forCellWithReuseIdentifier: "CreateCell")
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 177 * scaleW , height: 110 * scale)

        collectionView.collectionViewLayout = layout
    }
    @objc func didSelectBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        listData = JsonService.shared.readJsonPlaylist()
        collectionView.reloadData()
    }
}
extension PlayListController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listData.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "CreateCell", for: indexPath) as! CreateCell
        let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayListCell", for: indexPath) as! PlayListCell
        if indexPath.row == 0 {
            return cellA
        } else{
            let item = listData[indexPath.row - 1]
            cellB.lblTitle.text = item.name
            cellB.lblNumber.text = item.mSounds.count.description + " calls"
            return cellB
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            openAlert()
        }else {
            let vc = storyboard?.instantiateViewController(withIdentifier: ListCallsController.className) as! ListCallsController
            vc.modalPresentationStyle = .fullScreen
            vc.playlist = listData[indexPath.row - 1]
            present(vc, animated: true, completion: nil)
            vc.didSendData = {[weak self] in
                self?.listData = JsonService.shared.readJsonPlaylist()
                self?.collectionView.reloadData()
            }
        }
    }
    
    func openAlert(){
        let alertController = UIAlertController(title: "Name this playlist", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter name"
        }
        let saveAction = UIAlertAction(title: "Create", style: .default, handler: { alert -> Void in
            if let textField = alertController.textFields?[0] {
                if textField.text!.count > 0 {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: AddNewController.className) as! AddNewController
                    vc.modalPresentationStyle = .fullScreen
                    vc.name = textField.text
                    self.present(vc, animated: true, completion: nil)
                }
            }
        })
        if let popoverController = alertController.popoverPresentationController { // fix for ipad
          popoverController.sourceView = self.view
          popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
          popoverController.permittedArrowDirections = []
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(action : UIAlertAction!) -> Void in })
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

//extension PlayListController: UICollectionViewDelegateFlowLayout{
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let colWidth = UIScreen.main.bounds.width
//        return CGSize(width: colWidth / 2, height: 110 * scale)
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 15
//    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//}
