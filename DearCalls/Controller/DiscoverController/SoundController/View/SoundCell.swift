//
//  SoundCell.swift
//  DearCalls
//
//  Created by dovietduy on 12/5/20.
//

import UIKit

class SoundCell: UITableViewCell {
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgChartBar: UIImageView!
    var item: SoundModel!
    var indexPath: IndexPath!
    var delegate: SoundCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func prepareForReuse() {
        btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_play"), for: .normal)
        imgChartBar.isHidden = true
    }
    @IBAction func didSelectBtnPlay(_ sender: Any) {
        delegate?.didSelectBtnPlay(cell: self)
    }
}

protocol SoundCellDelegate: class {
    func didSelectBtnPlay(cell: SoundCell)
}
