//
//  SoundController.swift
//  DearCalls
//
//  Created by dovietduy on 12/5/20.
//

import UIKit

class SoundController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var viewSetting: UIView!
    var data: AnimalModel!
    var vSpinner: UIView!
    var audioPlayer: AudioPlayer!
    var githubService: GithubService!
    var isPlaying = false
    var indexPlaying: IndexPath!
    var beforeIndexPlaying: IndexPath!
    var isLightUp = false
    var settings = SettingModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        viewSetting.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnSettings(_:))))
        tblView.dataSource = self
        tblView.delegate = self
        tblView.register(UINib(nibName: "SoundCell", bundle: nil), forCellReuseIdentifier: "SoundCell")
        lblTitle.text = data.animalName
        lblDescription.text = data.Description
        imgView.image = UIImage(named: data.image)
        audioPlayer = AudioPlayer()
        audioPlayer.delegate = self
        githubService = GithubService()
        githubService.delegate = self
        setting()
    }
    @objc func didSelectBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        audioPlayer.stopAudio()
        DispatchQueue.main.async {
            self.audioPlayer.stopAudio()
        }
    }
    @IBAction func didSelectBtnMoon(_ sender: Any) {
        if isLightUp {
            UIScreen.main.brightness = 0.0
        } else {
            UIScreen.main.brightness = 1.0
        }
        isLightUp = !isLightUp
    }
    @objc func didSelectBtnSettings(_ sender: Any) {
        audioPlayer.stopAudio()
        if let indexPlaying = indexPlaying{
            guard let cell = cellForRowAt(indexPath: indexPlaying) else {return}
            cell.btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_play"), for: .normal)
            cell.imgChartBar.isHidden = true
        }
        DispatchQueue.main.async {
            self.audioPlayer.stopAudio()
        }
        let vc = storyboard?.instantiateViewController(withIdentifier: "SoundSettingController") as! SoundSettingController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
        vc.onComplete = {[weak self] in
            self?.setting()
        }
    }
    func setting(){
        if let temp = UserDefaults.standard.value(forKey: "isRepeat") as? Bool{
            settings.isRepeat = temp
            if let temp = UserDefaults.standard.value(forKey: "repeatDuration") as? Double{
                settings.repeatDuration = temp.format("00.00")
                settings.repeatType = "Infinite"
            }
        }
        if let temp = UserDefaults.standard.value(forKey: "isDelay") as? Bool{
            settings.isDelay = temp
            if let temp = UserDefaults.standard.value(forKey: "delayDuration") as? Double{
                settings.delayDuration = temp.format("00.00")
            }
        }
    }
}
extension SoundController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.mSounds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SoundCell", for: indexPath) as! SoundCell
        let item = data.mSounds[indexPath.row]
        cell.lblTitle.text = item.Description
        cell.lblTime.text = item.timeFormat() + " (min:sec)"
        cell.item = item
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85 * scale
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectRowAtOrBtnPlay(indexPath: indexPath)
    }
    
    func cellForRowAt(indexPath: IndexPath) -> SoundCell? {
        guard let cell = tblView.cellForRow(at: indexPath) as? SoundCell else {
            return tblView.dequeueReusableCell(withIdentifier: "SoundCell", for: indexPath) as? SoundCell
        }
        return cell
    }
    
}

extension SoundController: GithubServiceDelegate{
    func didDownloadFiles(fileURLs: [URL]) {
        
    }
    
    func didDownloadFile(fileURL: URL) {
        removeSpinner()
        if isPlaying{
            audioPlayer.stopAudio()
            guard let cell = cellForRowAt(indexPath: beforeIndexPlaying) else {return}
            cell.btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_play"), for: .normal)
            cell.imgChartBar.isHidden = true
        } else {
            audioPlayer.playAudio(fileURL: fileURL, settings)
            guard let cell = cellForRowAt(indexPath: indexPlaying) else {return}
            cell.btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_pause"), for: .normal)
            cell.imgChartBar.isHidden = false
            beforeIndexPlaying = indexPlaying
        }
        isPlaying = !isPlaying
        if beforeIndexPlaying != indexPlaying{
            didDownloadFile(fileURL: fileURL)
        }
    }
}
extension SoundController: AudioPlayerDelegate{
    func didFinishPlaying() {
        if !settings.isRepeat{
            guard let cell = cellForRowAt(indexPath: indexPlaying) else {return}
            cell.btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_play"), for: .normal)
            cell.imgChartBar.isHidden = true
        }
        
    }
}
extension SoundController: SoundCellDelegate{
    func didSelectBtnPlay(cell: SoundCell) {
        didSelectRowAtOrBtnPlay(indexPath: cell.indexPath)
    }
    
    func didSelectRowAtOrBtnPlay(indexPath: IndexPath){
        guard let cell = cellForRowAt(indexPath: indexPath) else {return}
        showSpinner(onView: cell.btnPlay)
        indexPlaying = indexPath
        let item = data.mSounds[indexPath.row]
        let fileName = (item.soundFile).sha1() + ".mp3"
        githubService.getData(fileName: fileName)
    }
}
extension SoundController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        spinnerView.layer.cornerRadius = spinnerView.bounds.width / 2
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        vSpinner = spinnerView
    }
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}
