//
//  SoundSettingController.swift
//  DearCalls
//
//  Created by dovietduy on 12/28/20.
//

import UIKit

class SoundSettingController: UIViewController {
    @IBOutlet weak var viewRepeat: UIView!
    @IBOutlet weak var viewRepeatDuration: UIView!
    @IBOutlet weak var viewDelay: UIView!
    @IBOutlet weak var viewDelayDuration: UIView!
    @IBOutlet weak var imgBox1: UIImageView!
    @IBOutlet weak var imgBox2: UIImageView!
    @IBOutlet weak var lblRepeatDuration: UILabel!
    @IBOutlet weak var lblDelayDuration: UILabel!
    @IBOutlet weak var viewBack: UIView!
    var isRepeat = false
    var isDelay = false
    var repeatDuration = 0.0
    var delayDuration = 0.0
    var onComplete: (() -> ())?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        viewDelay.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewDelay(_:))))
        viewDelayDuration.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewDelayDuration(_:))))
        viewRepeat.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewRepeat(_:))))
        viewRepeatDuration.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewRepeatDuration(_:))))
        if let temp = getSetting("isRepeat") as? Bool {
            isRepeat = temp
            if isRepeat{
                imgBox1.image = #imageLiteral(resourceName: "ic_tick")
                viewRepeatDuration.alpha = 1.0
                if let temp = getSetting("repeatDuration") as? Double{
                    repeatDuration = temp
                    lblRepeatDuration.text = repeatDuration.format("00.00").replacingOccurrences(of: ".", with: ":").replacingOccurrences(of: ",", with: ":") + " (min:sec)"
                }
            }else{
                imgBox1.image = #imageLiteral(resourceName: "ic_box")
                viewRepeatDuration.alpha = 0.5
                if let temp = getSetting("repeatDuration") as? Double{
                    repeatDuration = temp
                    lblRepeatDuration.text = repeatDuration.format("00.00").replacingOccurrences(of: ".", with: ":").replacingOccurrences(of: ",", with: ":") + " (min:sec)"
                }
            }
        }
        if let temp = getSetting("isDelay") as? Bool {
            isDelay = temp
            if isDelay{
                imgBox2.image = #imageLiteral(resourceName: "ic_tick")
                viewDelayDuration.alpha = 1.0
                if let temp = getSetting("delayDuration") as? Double{
                    delayDuration = temp
                    lblDelayDuration.text = delayDuration.format("00.00").replacingOccurrences(of: ".", with: ":").replacingOccurrences(of: ",", with: ":") + " (min:sec)"
                }
            }else{
                imgBox2.image = #imageLiteral(resourceName: "ic_box")
                viewDelayDuration.alpha = 0.5
                if let temp = getSetting("delayDuration") as? Double{
                    delayDuration = temp
                    lblDelayDuration.text = delayDuration.format("00.00").replacingOccurrences(of: ".", with: ":").replacingOccurrences(of: ",", with: ":") + " (min:sec)"
                }
            }
        }
    }
    @objc func didSelectViewRepeat(_ sender: UITapGestureRecognizer){
        isRepeat = !isRepeat
        saveSettings(isRepeat, "isRepeat")
        if isRepeat{
            imgBox1.image = #imageLiteral(resourceName: "ic_tick")
            viewRepeatDuration.alpha = 1.0
        } else {
            imgBox1.image = #imageLiteral(resourceName: "ic_box")
            viewRepeatDuration.alpha = 0.5
        }
    }
    @objc func didSelectViewRepeatDuration(_ sender: UITapGestureRecognizer){
        if isRepeat{
            let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpController") as! PopUpController
            vc.modalPresentationStyle = .overFullScreen
            self.repeatDuration = getSetting("repeatDuration") as! Double
            let str = repeatDuration.format("00.00").replacingOccurrences(of: ".", with: ":").replacingOccurrences(of: ",", with: ":")
            let strs = str.split(separator: ":")
            vc.strMinutes = String(strs[0])
            vc.strSeconds = String(strs[1])
            present(vc, animated: true, completion: nil)
            vc.didSendData = {[weak self] str in
                if let temp = Double(str) {
                    self!.repeatDuration = temp
                    self!.saveSettings(temp, "repeatDuration")
                    self!.lblRepeatDuration.text = temp.format("00.00").replacingOccurrences(of: ".", with: ":").replacingOccurrences(of: ",", with: ":") + " (min:sec)"
                }
            }
        }
    }
    @objc func didSelectViewDelay(_ sender: UITapGestureRecognizer){
        isDelay = !isDelay
        saveSettings(isDelay, "isDelay")
        if isDelay{
            imgBox2.image = #imageLiteral(resourceName: "ic_tick")
            viewDelayDuration.alpha = 1.0
        } else {
            imgBox2.image = #imageLiteral(resourceName: "ic_box")
            viewDelayDuration.alpha = 0.5
        }
    }
    @objc func didSelectViewDelayDuration(_ sender: UITapGestureRecognizer){
        if isDelay{
            let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpController") as! PopUpController
            vc.modalPresentationStyle = .overFullScreen
            self.delayDuration = getSetting("delayDuration") as! Double
            let str = delayDuration.format("00.00").replacingOccurrences(of: ".", with: ":").replacingOccurrences(of: ",", with: ":")
            let strs = str.split(separator: ":")
            vc.strMinutes = String(strs[0])
            vc.strSeconds = String(strs[1])
            present(vc, animated: true, completion: nil)
            vc.didSendData = {[weak self] str in
                if let temp = Double(str) {
                    self!.delayDuration = temp
                    self!.saveSettings(temp, "delayDuration")
                    self!.lblDelayDuration.text = temp.format("00.00").replacingOccurrences(of: ".", with: ":").replacingOccurrences(of: ",", with: ":") + " (min:sec)"
                }
            }
        }
    }
    func saveSettings(_ value: Any,_ key: String){
        UserDefaults.standard.setValue(value, forKey: key)
    }
    func getSetting(_ key: String) -> Any{
        return UserDefaults.standard.value(forKey: key) as Any
    }
    @objc func didSelectBtnBack(_ sender: Any){
        dismiss(animated: true, completion: nil)
        onComplete?()
    }
}
