//
//  DiscoverController.swift
//  DearCalls
//
//  Created by dovietduy on 12/2/20.
//

import UIKit
import UPCarouselFlowLayout
import StoreKit
import GoogleMobileAds
let isHuntingCall = 0
let isSolunarTime = 1
let isWeather = 2
let isActivityLog = 3
let listCategory = ["img_card_hunting_calls", "img_card_solunar", "img_card_weather", "img_card_log"]

class DiscoverController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var topCollectionView: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        topCollectionView.constant = 137 * scale
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "DiscoverCell", bundle: nil), forCellWithReuseIdentifier: "DiscoverCell")
        
        let layout = UPCarouselFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: 340 * scale, height: 540 * scale)
        layout.spacingMode = .fixed(spacing: 10.0)
        layout.sideItemScale = 0.8
        layout.sideItemAlpha = 1.0
        collectionView.collectionViewLayout = layout
        if PaymentManager.shared.isPurchase(){
            
        }else{
            AdmobManager.shared.loadBannerView(inVC: self)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if #available(iOS 10.3, *) {
//            SKStoreReviewController.requestReview()
//        } else {
//
//        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if PaymentManager.shared.isPurchase(){
            
        }else{
            AdmobManager.shared.loadAdFull(inVC: self)
        }
    }
}

extension DiscoverController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listCategory.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscoverCell", for: indexPath) as! DiscoverCell
        cell.imgCard.image = UIImage(named: listCategory[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case isHuntingCall:
            
            let vc = storyboard?.instantiateViewController(withIdentifier: HuntingCallsController.className) as! HuntingCallsController
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
            
            
        case isSolunarTime:
            
            let vc = storyboard?.instantiateViewController(withIdentifier: TabBarSolunarController.className) as! TabBarSolunarController
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
            
            
        case isWeather:
            
            let vc = storyboard?.instantiateViewController(withIdentifier: WeatherController.className) as! WeatherController
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
            
            
        case isActivityLog:
            
            let vc = storyboard?.instantiateViewController(withIdentifier: ActivityController.className) as! ActivityController
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
           
        default:
            print("not found")
        }
    }

}
