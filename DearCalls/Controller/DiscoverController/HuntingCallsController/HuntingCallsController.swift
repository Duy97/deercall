//
//  HuntingCallsController.swift
//  DearCalls
//
//  Created by dovietduy on 12/3/20.
//

import UIKit
import StoreKit
import GoogleMobileAds
class HuntingCallsController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnPlaylist: UIButton!
    @IBOutlet weak var lblPlaylist: UILabel!
    @IBOutlet weak var lblPlaylist2: UILabel!
    @IBOutlet weak var viewBack: UIView!
    
    var listData = JsonService.shared.readJsonFile()
    var admobNativeAds: GADUnifiedNativeAd?
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.dataSource = self
        tblView.delegate = self
        tblView.register(UINib(nibName: "HuntingCallsCell", bundle: nil), forCellReuseIdentifier: "HuntingCallsCell")
        tblView.register(UINib(nibName: "nativeAdmobTBLCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "nativeAdmobTBLCell")
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        lblPlaylist.text = listData[0].animalName
        lblPlaylist2.text = listData[0].Description
        listData.remove(at: 0)
        listData.remove(at: 0)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if PaymentManager.shared.isPurchase(){
            
        }else{
            if let native = AdmobManager.shared.randoomNativeAds(){
                admobNativeAds = native as? GADUnifiedNativeAd
                tblView.reloadData()
            }
        }
    }
    @objc func didSelectBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func didSelectHuntingPlaylist(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: PlayListController.className) as! PlayListController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
}
extension HuntingCallsController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HuntingCallsCell", for: indexPath) as! HuntingCallsCell
        let item = listData[indexPath.row]
        cell.lblTitle.text = item.animalName
        cell.lblDescription.text = item.Description
        cell.imgView.image = UIImage(named: item.image)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 212 * scale
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: SoundController.className) as! SoundController
        vc.modalPresentationStyle = .fullScreen
        vc.data = listData[indexPath.row]
        present(vc, animated: true, completion: nil)
    }
}

extension HuntingCallsController{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "nativeAdmobTBLCell") as! nativeAdmobTBLCell
        if let native = self.admobNativeAds {
            headerView.setupHeader(nativeAd: native)
        }
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if admobNativeAds == nil{
            return 0
        }
        return 160 * scale
    }
}
