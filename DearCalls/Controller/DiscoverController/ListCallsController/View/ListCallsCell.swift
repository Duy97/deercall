//
//  ListCallsCell.swift
//  DearCalls
//
//  Created by dovietduy on 12/7/20.
//

import UIKit

class ListCallsCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDelayDuration: UILabel!
    @IBOutlet weak var imgIconRepeat: UIImageView!
    @IBOutlet weak var lblRepeatDuration: UILabel!
    @IBOutlet weak var lblVolume: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
