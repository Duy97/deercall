//
//  ListCallsController.swift
//  DearCalls
//
//  Created by dovietduy on 12/7/20.
//

import UIKit

class ListCallsController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnRepeat: UIButton!
    @IBOutlet weak var btnOverlap: UIButton!
    @IBOutlet weak var viewBtnAdd: UIView!
    @IBOutlet weak var viewBtnEdit: UIView!
    @IBOutlet weak var viewBtnDelete: UIView!
    @IBOutlet weak var viewBtnHelp: UIView!
    @IBOutlet weak var viewBack: UIView!
    var vSpinner: UIView!
    var githubService: GithubService!
    var isLightUp = true
    var playlist: PlaylistModel!
    var audioPlayer: AudioPlayer!
    var gsAudio: GSAudio!
    var indexPlaying = IndexPath(row: 0, section: 0)
    var isPlaying = false
    var isRepeat = false
    var isOverlap = false
    var isInfinte = false
    var didSendData: (() -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.dataSource = self
        tblView.delegate = self
        tblView.register(UINib(nibName: "ListCallsCell", bundle: nil), forCellReuseIdentifier: "ListCallsCell")
        lblTitle.text = playlist.name
        audioPlayer = AudioPlayer()
        audioPlayer.delegate = self
        githubService = GithubService()
        githubService.delegate = self
        gsAudio = GSAudio()
        viewBtnAdd.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnAdd(_ :))))
        viewBtnEdit.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnEdit(_ :))))
        viewBtnDelete.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnDelete(_ :))))
        viewBtnHelp.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnHelp(_ :))))
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
    }
    @objc func didSelectBtnAdd(_ sender: UITapGestureRecognizer){
        let addVC = storyboard?.instantiateViewController(withIdentifier: "AddNewController") as! AddNewController
        addVC.modalPresentationStyle = .fullScreen
        addVC.name = lblTitle.text
        addVC.playlist = playlist
        addVC.listAdd = playlist.mSounds
        addVC.isNewPlaylist = false
        addVC.didSelectBtnAdd = {[weak self] playlist  in
            self?.playlist = playlist
            self?.tblView.reloadData()
        }
        present(addVC, animated: true, completion: nil)
    }
    @objc func didSelectBtnEdit(_ sender: UITapGestureRecognizer){
        let vc = storyboard?.instantiateViewController(withIdentifier: "EditPlaylistController") as! EditPlaylistController
        vc.modalPresentationStyle = .fullScreen
        vc.playlist = playlist
        vc.didSelectBtnDelete = {[weak self] playlist  in
            self?.playlist = playlist
            self?.tblView.reloadData()
        }
        present(vc, animated: true, completion: nil)
    }
    
    @objc func didSelectBtnHelp(_ sender: UITapGestureRecognizer){
        
    }
    @objc func didSelectBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        audioPlayer.stopAudio()
        gsAudio.stopAll()
        didSendData?()
    }
    @IBAction func didSelectBtnRename(_ sender: Any) {
        audioPlayer.stopAudio()
        gsAudio.stopAll()
        openAlert()
    }
    @IBAction func didSelectBtnMoon(_ sender: Any) {
        if isLightUp {
            UIScreen.main.brightness = 0.0
        } else {
            UIScreen.main.brightness = 1.0
        }
        isLightUp = !isLightUp
    }
    
    @IBAction func didSelectBtnRepeat(_ sender: Any) {
        if isRepeat{
            self.view.showToast(message: "Repeat OFF \n The playlist will NOT repeat after \n it is finished playing.")
            btnRepeat.backgroundColor = .clear
        } else{
            self.view.showToast(message: "Repeat ON \n The playlist will repeat infinitely after \n it is finished playing.")
            btnRepeat.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
        isRepeat = !isRepeat
        
    }
    @IBAction func didSelectBtnOverlap(_ sender: Any) {
        if isOverlap{
            self.view.showToast(message: "Overlap OFF \n Calls won't overlap each other. \n Hold items and drag to desired order. \n Use delay to set silence time \n between each call.")
            btnOverlap.backgroundColor = .clear
        } else{
            self.view.showToast(message: "Overlap ON \n Calls can overlap and they are \n sorted by delay time.")
            btnOverlap.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            
        }
        isOverlap = !isOverlap
    }
}
extension ListCallsController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlist.mSounds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCallsCell", for: indexPath) as! ListCallsCell
        let item = playlist.mSounds[indexPath.row]
        let itemSetting = item.setting
        cell.lblTitle.text = item.Description
        cell.lblTime.text = item.timeFormat() + " (min:sec)"
        if itemSetting.isDelay == true {
            cell.lblDelayDuration.text = itemSetting.delayDuration.format()
        } else{
            cell.lblDelayDuration.text = "OFF"
        }
        if itemSetting.isRepeat == true {
            cell.lblRepeatDuration.text = itemSetting.repeatDuration.format()
            cell.imgIconRepeat.image = UIImage(named: itemSetting.icon)
            print(itemSetting.icon)
        } else{
            cell.lblRepeatDuration.text = "OFF"
            cell.imgIconRepeat.image = UIImage(named: "ic_refresh")
        }
        cell.lblVolume.text = itemSetting.volume.description + "%"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85 * scale
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DurationController") as! DurationController
        vc.mSound = playlist.mSounds[indexPath.row]
        vc.playlist = playlist
        vc.index = indexPath.row
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
        vc.didSendData = {[weak self] playlist in
            self?.playlist = playlist
            JsonService.shared.writeOjectToJson(playlist: (self?.playlist)!)
            self?.tblView.reloadData()
        }
    }
}
extension ListCallsController: GithubServiceDelegate{
    func didDownloadFiles(fileURLs: [URL]) {
        gsAudio.playSounds(soundFileNames: fileURLs)
        removeSpinner()
    }
    
    func didDownloadFile(fileURL: URL) {
        removeSpinner()
        btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_pause_2"), for: .normal)
        let setup = playlist.mSounds[indexPlaying.row].setting
        audioPlayer.onDelaying = {[weak self] message in
            self?.view.showToast(message: message)
        }
        audioPlayer.onInfinite = {[weak self] on in
            self?.isInfinte = on
        }
        audioPlayer.onTimed = {[weak self] message in
            self?.isPlaying = false
            self?.audioPlayer.stopAudio()
            self?.btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_play_2"), for: .normal)
            self?.view.showToast(message: message)
        }
        audioPlayer.playAudio(fileURL: fileURL, setup)
        
    }
}
// play audio
extension ListCallsController: AudioPlayerDelegate{
    @IBAction func didSelectBtnPlay(_ sender: Any) {
        if isPlaying {
            audioPlayer.stopAudio()
            btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_play_2"), for: .normal)
        }else{
            if isOverlap{
                showSpinner(onView: btnPlay)
                var listfileName: [String] = []
                for sound in playlist.mSounds{
                    let fileName = (sound.soundFile).sha1() + ".mp3"
                    listfileName.append(fileName)
                }
                githubService.getData(fileNames: listfileName)
            } else{
                showSpinner(onView: btnPlay)
                let item = playlist.mSounds[indexPlaying.row]
                let fileName = (item.soundFile).sha1() + ".mp3"
                githubService.getData(fileName: fileName)
            }
        }
        isPlaying = !isPlaying
    }
    
    func didFinishPlaying() {
        if isInfinte == false {
            btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_play_2"), for: .normal)
        } else{
            btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_pause_2"), for: .normal)
        }
        indexPlaying.row += 1
        if indexPlaying.row < playlist.mSounds.count{
            showSpinner(onView: btnPlay)
            let item = playlist.mSounds[indexPlaying.row]
            let fileName = (item.soundFile).sha1() + ".mp3"
            githubService.getData(fileName: fileName)
        } else{
            indexPlaying.row = 0
            if isRepeat{
                showSpinner(onView: btnPlay)
                let item = playlist.mSounds[indexPlaying.row]
                let fileName = (item.soundFile).sha1() + ".mp3"
                githubService.getData(fileName: fileName)
            }
        }
        
    }
}

extension ListCallsController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        spinnerView.layer.cornerRadius = spinnerView.bounds.width / 2
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        vSpinner = spinnerView
    }
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}
extension ListCallsController{
    func openAlert(){
        let alertController = UIAlertController(title: "Rename playlist", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter name"
        }
        let saveAction = UIAlertAction(title: "Rename", style: .default, handler: { alert -> Void in
            if let textField = alertController.textFields?[0] {
                if textField.text!.count > 0 {
                    self.playlist.name = textField.text!
                    JsonService.shared.writeOjectToJson(playlist: self.playlist)
                    self.lblTitle.text = textField.text!
                }
            }
        })
        if let popoverController = alertController.popoverPresentationController { // fix for ipad
          popoverController.sourceView = self.view
          popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
          popoverController.permittedArrowDirections = []
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(action : UIAlertAction!) -> Void in })
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
extension ListCallsController{
    @objc func didSelectBtnDelete(_ sender: UITapGestureRecognizer){
        let alertController = UIAlertController(title: "Delete Playlist", message: "Delete \(playlist.name)? This cannot be undone", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "YES", style: .default, handler: { alert -> Void in
            var documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentURL.appendPathComponent(self.playlist.name + "(playlist).json")
            do {
                try FileManager.default.removeItem(atPath: documentURL.path)
                self.dismiss(animated: true, completion: nil)
            } catch {
                print(error.localizedDescription)
            }
        })
        if let popoverController = alertController.popoverPresentationController { // fix for ipad
          popoverController.sourceView = self.view
          popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
          popoverController.permittedArrowDirections = []
        }
        let cancelAction = UIAlertAction(title: "NO", style: .cancel, handler: {(action : UIAlertAction!) -> Void in })
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func deleteFileJson(){
        
    }
}
