//
//  ActivityCell.swift
//  DearCalls
//
//  Created by dovietduy on 12/22/20.
//

import UIKit
import MapKit

class ActivityCell: UICollectionViewCell {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lblHour: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    var item: LogModel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setup(_ item: LogModel){
        self.item = item
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: item.latitue, longitude: item.longtitue)
        mapView.addAnnotation(annotation)
        let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
        mapView.setRegion(region, animated: true)
        
        lblDay.text = item.day
        lblHour.text = item.hour
    }
    
}
