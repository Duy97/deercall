//
//  ActivityController.swift
//  DearCalls
//
//  Created by dovietduy on 12/22/20.
//

import UIKit
import MapKit

class ActivityController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblNew: UILabel!
    @IBOutlet weak var viewNew: UIView!
    @IBOutlet weak var viewBack: UIView!
    
    var listData: [LogModel] = JsonService.shared.readJsonLog()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "ActivityCell", bundle: nil), forCellWithReuseIdentifier: "ActivityCell")
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 177 * scaleW , height: 170 * scale)
        collectionView.collectionViewLayout = layout
        viewNew.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectLblNew(_:))))
    }
    override func viewWillAppear(_ animated: Bool) {
        listData = JsonService.shared.readJsonLog()
        collectionView.reloadData()
    }
    @objc func didSelectLblNew(_ sender: UITapGestureRecognizer){
        let alert = UIAlertController()
        alert.title = "Select Radar Layer"
        let action1 = UIAlertAction(title: "Current GPS Coords", style: .default) { (act) in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogController") as! LogController
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .fullScreen
            vc.isCurrentLocation = true
            self.present(vc, animated: true, completion: nil)
        }
        let action2 = UIAlertAction(title: "Location on Map", style: .default) { (act) in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            vc.modalPresentationStyle = .fullScreen
            vc.isPushedBy = .byLocationOnMap
            self.present(vc, animated: true, completion: nil)
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel) { (act) in}
        if let popoverController = alert.popoverPresentationController {
          popoverController.sourceView = self.view
          popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
          popoverController.permittedArrowDirections = []
        }
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(actionCancel)
        self.present(alert, animated: true, completion: nil)
    }
    @objc func didSelectBtnBack(_ sender: UIButton){
        dismiss(animated: true, completion: nil)
    }
}

extension ActivityController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActivityCell", for: indexPath) as! ActivityCell
        let item = listData[indexPath.row]
        cell.setup(item)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogController") as! LogController
        vc.modalPresentationStyle = .fullScreen
        vc.item = listData[indexPath.row]
        self.present(vc, animated: true, completion: nil)
    }
}
