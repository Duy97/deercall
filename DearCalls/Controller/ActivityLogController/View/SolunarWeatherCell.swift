//
//  SolunarWeatherCell.swift
//  DearCalls
//
//  Created by dovietduy on 12/22/20.
//

import UIKit

class SolunarWeatherCell: UITableViewCell {
    @IBOutlet weak var imgView: UIButton!
    @IBOutlet weak var lblTempurater: UILabel!
    @IBOutlet weak var btnRatingDay: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setup(_ item: LogModel){
        imgView.setBackgroundImage(UIImage(named: item.iconWeather), for: .normal)
        lblTempurater.text = Int(item.tempurature).description + "°"
        btnRatingDay.setTitle(item.dayRating.description + "/5", for: .normal)
    }
}
