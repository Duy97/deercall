//
//  CallsUsedCell.swift
//  DearCalls
//
//  Created by dovietduy on 12/22/20.
//

import UIKit

class CallsUsedCell: UITableViewCell {

    @IBOutlet weak var tableView: UITableView!
    var delegate: CallsUsedCellDelegate!
    var listSound: [SoundModel] = []
    var vSpinner: UIView!
    var audioPlayer: AudioPlayer!
    var githubService: GithubService!
    var isPlaying = false
    var indexPlaying: IndexPath!
    var beforeIndexPlaying: IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(stopAudio), name: Notification.Name("stopAudio"), object: nil)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "SoundCell", bundle: nil), forCellReuseIdentifier: "SoundCell")
        audioPlayer = AudioPlayer()
        audioPlayer.delegate = self
        githubService = GithubService()
        githubService.delegate = self
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func stopAudio(){
        audioPlayer.stopAudio()
    }
    @IBAction func didSelectBtnAdd(_ sender: Any) {
        delegate?.didSelectBtnAdd(self)
    }
    func setupListSound(_ sound: SoundModel){
        self.listSound.append(sound)
        tableView.reloadData()
    }
    func setup(_ item: LogModel){
        self.listSound = item.listCallUsed
        tableView.reloadData()
    }
    func cellForRowAt(indexPath: IndexPath) -> SoundCell? {
        guard let cell = tableView.cellForRow(at: indexPath) as? SoundCell else {
            return tableView.dequeueReusableCell(withIdentifier: "SoundCell", for: indexPath) as? SoundCell
        }
        return cell
    }
}
extension CallsUsedCell:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listSound.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SoundCell", for: indexPath) as! SoundCell
        let item = listSound[indexPath.row]
        cell.lblTitle.text = item.Description
        cell.lblTime.text = item.timeLength.format("00.00") + " mins"
        cell.item = item
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            listSound.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            delegate?.didDelete(self)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectRowAtOrBtnPlay(indexPath: indexPath)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85 * scale
    }
}
extension CallsUsedCell: GithubServiceDelegate{
    func didDownloadFiles(fileURLs: [URL]) {
        
    }
    
    func didDownloadFile(fileURL: URL) {
        removeSpinner()
        if isPlaying{
            audioPlayer.stopAudio()
            guard let cell = cellForRowAt(indexPath: beforeIndexPlaying) else {return}
            cell.btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_play"), for: .normal)
            cell.imgChartBar.isHidden = true
        } else {
            audioPlayer.playAudio(fileURL: fileURL)
            guard let cell = cellForRowAt(indexPath: indexPlaying) else {return}
            cell.btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_pause"), for: .normal)
            cell.imgChartBar.isHidden = false
            beforeIndexPlaying = indexPlaying
        }
        isPlaying = !isPlaying
        if beforeIndexPlaying != indexPlaying{
            didDownloadFile(fileURL: fileURL)
        }
    }
}
extension CallsUsedCell: AudioPlayerDelegate{
    func didFinishPlaying() {
        guard let cell = cellForRowAt(indexPath: indexPlaying) else {return}
        cell.btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_play"), for: .normal)
        cell.imgChartBar.isHidden = true
    }
}
extension CallsUsedCell: SoundCellDelegate{
    func didSelectBtnPlay(cell: SoundCell) {
        didSelectRowAtOrBtnPlay(indexPath: cell.indexPath)
    }
    
    func didSelectRowAtOrBtnPlay(indexPath: IndexPath){
        guard let cell = cellForRowAt(indexPath: indexPath) else {return}
        showSpinner(onView: cell.btnPlay)
        indexPlaying = indexPath
        let item = listSound[indexPath.row]
        let fileName = (item.soundFile).sha1() + ".mp3"
        githubService.getData(fileName: fileName)
    }
}
extension CallsUsedCell {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        spinnerView.layer.cornerRadius = spinnerView.bounds.width / 2
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        vSpinner = spinnerView
    }
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}

protocol CallsUsedCellDelegate: class {
    func didSelectBtnAdd(_ cell: CallsUsedCell)
    func didDelete(_ cell: CallsUsedCell)
}
