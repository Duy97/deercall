//
//  TimeCell.swift
//  DearCalls
//
//  Created by dovietduy on 12/22/20.
//

import UIKit

class TimeCell: UITableViewCell {

    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblHour: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }
    func setup(item: LogModel) {
        lblDay.text = item.day
        lblHour.text = item.hour
    }
}
