//
//  PlaylistLogCell.swift
//  DearCalls
//
//  Created by dovietduy on 12/22/20.
//

import UIKit

class PlaylistLogCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    var listData: [PlaylistModel] = []
    var delegate: PlayListLogCellDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "PlayListCell", bundle: nil), forCellWithReuseIdentifier: "PlayListCell")
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 177 * scale, height: 110 * scale)
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
    }
    @IBAction func didSelectBtnAdd(_ sender: Any) {
        delegate?.didSelectBtnAdd(self)
    }
    func setupListData(_ playListAdd: PlaylistModel){
        self.listData.append(playListAdd)
        collectionView.reloadData()
    }
    func setup(_ item: LogModel){
        self.listData = item.listPlayList
        collectionView.reloadData()
    }
}
extension PlaylistLogCell: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayListCell", for: indexPath) as! PlayListCell
        let item = listData[indexPath.row]
        cell.lblTitle.text = item.name
        cell.lblNumber.text = item.mSounds.count.description + " calls"
        cell.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(didDelete(_:))))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectRowAt(listData[indexPath.row])
    }
    @objc func didDelete(_ sender: UILongPressGestureRecognizer){
        if let cell = sender.view as? PlayListCell{
            if let indexPath = collectionView.indexPath(for: cell){
                listData.remove(at: indexPath.row)
                collectionView.reloadData()
                delegate?.didDelete(self)
            }
        }
        
    }
    
}

protocol PlayListLogCellDelegate: class {
    func didSelectBtnAdd(_ cell: PlaylistLogCell)
    func didSelectRowAt(_ playlist: PlaylistModel)
    func didDelete(_ cell: PlaylistLogCell)
}
