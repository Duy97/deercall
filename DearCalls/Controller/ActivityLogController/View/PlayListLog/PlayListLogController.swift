//
//  PlayListLogController.swift
//  DearCalls
//
//  Created by dovietduy on 12/24/20.
//

import UIKit

class PlayListLogController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewBack: UIView!
    var listData = JsonService.shared.readJsonPlaylist()
    var didAdd: ((_ listData: PlaylistModel) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "PlayListCell", bundle: nil), forCellWithReuseIdentifier: "PlayListCell")
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 177 * scaleW , height: 110 * scale)
        collectionView.collectionViewLayout = layout
    }
    @objc func didSelectBtnBack(_ sender: UIButton){
        dismiss(animated: true, completion: nil)
    }
}
extension PlayListLogController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlayListCell", for: indexPath) as! PlayListCell
        
        let item = listData[indexPath.row]
        cell.lblTitle.text = item.name
        cell.lblNumber.text = item.mSounds.count.description + " calls"
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didAdd?(listData[indexPath.row])
        view.showToast(message: "Added playlist to your activity log")
    }
}
