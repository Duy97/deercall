//
//  LogController.swift
//  DearCalls
//
//  Created by dovietduy on 12/22/20.
//

import UIKit
import CoreLocation
import Photos
enum Section: Int {
    case time = 0
    case location = 1
    case solution_weather = 2
    case photo = 3
    case note = 4
    case callUsed = 5
    case playlist = 6
}

class LogController: UIViewController, CLLocationManagerDelegate{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var btnDone: UIButton!
    var item = LogModel()
    var isCurrentLocation = false
    var isLocationOnMap = false
    var imagePicker = UIImagePickerController()
    var vSpinner: UIView!
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var onComplete: (()-> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        btnDone.titleLabel?.font = btnDone.titleLabel?.font.withSize(36 * scale)
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "LocationCell", bundle: nil), forCellReuseIdentifier: "LocationCell")
        tableView.register(UINib(nibName: "TimeCell", bundle: nil), forCellReuseIdentifier: "TimeCell")
        tableView.register(UINib(nibName: "SolunarWeatherCell", bundle: nil), forCellReuseIdentifier: "SolunarWeatherCell")
        tableView.register(UINib(nibName: "PhotoCell", bundle: nil), forCellReuseIdentifier: "PhotoCell")
        tableView.register(UINib(nibName: "CallsUsedCell", bundle: nil), forCellReuseIdentifier: "CallsUsedCell")
        tableView.register(UINib(nibName: "PlaylistLogCell", bundle: nil), forCellReuseIdentifier: "PlaylistLogCell")
        tableView.register(UINib(nibName: "NoteCell", bundle: nil), forCellReuseIdentifier: "NoteCell")
    }
    @objc func didSelectBtnBack(_ sender: UIButton){
        dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: Notification.Name("stopAudio"), object: nil)
        if isLocationOnMap{
            onComplete?()
        }
    }
    @IBAction func didSelectBtnDone(_ sender: Any) {
        JsonService.shared.writeOjectToJson(item: item)
        dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: Notification.Name("stopAudio"), object: nil)
        if isLocationOnMap{
            onComplete?()
        }
    }
    func setupLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    var currently: TodayModel!
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !locations.isEmpty, currentLocation == nil  {
            currentLocation = locations.first
            locationManager.stopUpdatingLocation()
            showSpinner(onView: tableView.cellForRow(at: IndexPath(row: 0, section: 2)) ?? tableView)
            guard let currentLocation = currentLocation else {
                return
            }
            let lng = currentLocation.coordinate.longitude
            let lat = currentLocation.coordinate.latitude
            APIService.shared.GetFullAPI(lat.description, lng.description) { [self] (current, listDay, timezone, error) in
                if let temp = current as? TodayModel {self.currently = temp}
                if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 2)) as? SolunarWeatherCell{
                    cell.lblTempurater.text = Int(self.currently.temperature).description + "°"
                    if let image = UIImage(named: self.currently.icon) {
                        cell.imgView.setBackgroundImage(image, for: .normal)
                        self.item.iconWeather = self.currently.icon
                        self.item.tempurature = self.currently.temperature
                    }
                }
                self.removeSpinner()
            }
            APIService.shared.GetSoluarAPI(lat.description, lng.description, getDateForAPI(Date()), getTimeZone()) { (data, error) in
                if let data = data as? SolunarModel{
                    if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 2)) as? SolunarWeatherCell{
                        cell.btnRatingDay.setTitle(data.dayRating.description + "/5", for: .normal)
                        self.item.dayRating = data.dayRating
                    }
                }
            }
        }
    }
    func getDateForAPI(_ date: Date?) -> String{
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYYMMdd"
        return formatter.string(from: inputDate)
    }
    func getTimeZone() -> String{
        var localTimeZoneAbbreviation: String { return TimeZone.current.abbreviation() ?? "" }
        let strs = localTimeZoneAbbreviation.split(separator: "+")
        if strs.count == 2{
            return String(strs[1])
        }
        let str2s = localTimeZoneAbbreviation.split(separator: "-")
        if str2s.count == 2 {
            return "-" + str2s[1]
        }
        return ""
    }
    func getDayOfDate(_ date: Date?) -> String{
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, d MMM YYYY"
        return formatter.string(from: inputDate)
    }
    func getHourOfDate(_ date: Date?) -> String{
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm:ss a"
        return formatter.string(from: inputDate)
    }
}

extension LogController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Section(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        switch section {
        case .time:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimeCell") as! TimeCell
            if isCurrentLocation || isLocationOnMap{
                let now = Date()
                cell.lblDay.text = getDayOfDate(now)
                cell.lblHour.text = getHourOfDate(now)
                item.day = getDayOfDate(now)
                item.hour = getHourOfDate(now)
            } else{
                cell.setup(item: item)
            }
            return cell
        case .location:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell") as! LocationCell
            cell.delegate = self
            if isCurrentLocation{
                cell.getCurrentLoction()
            } else{
                cell.setup(item)
            }
            return cell
        case .solution_weather:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SolunarWeatherCell") as! SolunarWeatherCell
            if isCurrentLocation || isLocationOnMap{
                setupLocation()
            } else{
                cell.setup(item)
            }
            return cell
        case .photo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell") as! PhotoCell
            cell.listImage = item.listImage
            cell.delegate = self
            cell.btnAdd.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewAdd(_:))))
            return cell
        case .note:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell") as! NoteCell
            cell.delegate = self
            cell.setup(item)
            return cell
        case .callUsed:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CallsUsedCell") as! CallsUsedCell
            cell.delegate = self
            cell.setup(item)
            return cell
        case .playlist:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlaylistLogCell") as! PlaylistLogCell
            cell.delegate = self
            cell.setup(item)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let section = Section(rawValue: indexPath.section) else {
            return 0
        }
        switch section {
        case .time:
            return 115 * scale
        case .location:
            return 300 * scale
        case .solution_weather:
            return 140 * scale
        case .photo:
            return 190 * scale
        case .note:
            return 227 * scale
        case .callUsed:
            return 250 * scale
        case .playlist:
            return 250 * scale
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let section = Section(rawValue: indexPath.section) else {
            return
        }
        switch section {
        case .time:
            break
        case .location:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            vc.modalPresentationStyle = .fullScreen
            vc.isPushedBy = isCurrentLocation ? .byCurrentLoctionOnLogVC : .byLogVC
            vc.item = item
            self.present(vc, animated: true, completion: nil)
            break
        case .solution_weather:
            break
        case .photo:
            break
        case .note:
            break
        case .callUsed:
            break
        case .playlist:
            break
        }
    }
    
}
extension LogController: PhotoCellDelegate{
    func didDelete(_ cell: PhotoCell) {
        self.item.listImage = cell.listImage
    }
}
extension LogController: NoteCellDelegate{
    func didEndEditing(text: String) {
        item.note = text
    }
}
extension LogController: LocationCellDelegate{
    func didGetCurrentLocation(latitute: Double, longtitute: Double) {
        item.latitue = latitute
        item.longtitue = longtitute
    }
}
extension LogController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    @objc func didSelectViewAdd(_ sender: UITapGestureRecognizer){
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imageURL = info[UIImagePickerController.InfoKey.imageURL] as? URL {
            guard let cell = tableView.cellForRow(at: IndexPath(row: 0, section: Section.photo.rawValue)) as? PhotoCell else{return}
            let url = copyImage(imageURL)
            cell.addNewPhoto(url)
            self.item.listImage = cell.listImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func copyImage(_ url: URL) -> URL {
        var newURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        newURL.appendPathComponent(url.lastPathComponent)
        do {
            try FileManager.default.copyItem(atPath: url.path, toPath: newURL.path)
        } catch{
            
        }
        return newURL
    }
}
extension LogController: CallsUsedCellDelegate{
    func didSelectBtnAdd(_ cell: CallsUsedCell) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddNewController") as! AddNewController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .fullScreen
        vc.isPresentByLogVC = true
        vc.name = "your activity log"
        present(vc, animated: true, completion: nil)
        vc.didSelectBtnAddInLogVC = {[weak self] (listAdd) in
            guard let cell = self?.tableView.cellForRow(at: IndexPath(row: 0, section: Section.callUsed.rawValue)) as? CallsUsedCell else{return}
            cell.setupListSound(listAdd)
            self?.item.listCallUsed = cell.listSound
        }
    }
    func didDelete(_ cell: CallsUsedCell) {
        self.item.listCallUsed = cell.listSound
    }
}
extension LogController: PlayListLogCellDelegate{
    func didSelectRowAt(_ playlist: PlaylistModel) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ListCallsController") as! ListCallsController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .fullScreen
        vc.playlist = playlist
        present(vc, animated: true, completion: nil)
    }
    
    func didSelectBtnAdd(_ cell: PlaylistLogCell) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PlayListLogController") as! PlayListLogController
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
        vc.didAdd = {[weak self] (playlistAdd) in
            guard let cell = self?.tableView.cellForRow(at: IndexPath(row: 0, section: Section.playlist.rawValue)) as? PlaylistLogCell else{return}
            cell.setupListData(playlistAdd)
            self?.item.listPlayList = cell.listData
        }
    }
    func didDelete(_ cell: PlaylistLogCell) {
        self.item.listPlayList = cell.listData
    }
}
extension LogController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        vSpinner = spinnerView
    }
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}
