//
//  WeatherCell.swift
//  DearCalls
//
//  Created by dovietduy on 12/8/20.
//

import UIKit

class WeatherCell: UITableViewCell {

    @IBOutlet weak var lblTempurature: UILabel!
    @IBOutlet weak var lblT: UILabel!
    @IBOutlet weak var lblU: UILabel!
    @IBOutlet weak var lblE: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
