//
//  WeatherController.swift
//  DearCalls
//
//  Created by dovietduy on 12/8/20.
//

import UIKit
import CoreLocation

class WeatherController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblCurrentTemputure: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblSeason: UILabel!
    @IBOutlet weak var viewBack: UIView!
    var vSpinner: UIView!
    var currently = TodayModel()
    var daily = [DailyModel]()
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var timezone = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: Notification.Name("setup"), object: nil)
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        tblView.dataSource = self
        tblView.delegate = self
        tblView.register(UINib(nibName: "WeatherCell", bundle: nil), forCellReuseIdentifier: "WeatherCell")
    }
    @objc func refresh(){
        tblView.reloadData()
        requestWeatherForLocation()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @IBAction func didSelectBtnSetting(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SolunarSettingController") as! SolunarSettingController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupLocation()
    }
    func setupLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    var city = ""
    var country = ""
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !locations.isEmpty, currentLocation == nil  {
            currentLocation = locations.first
            locationManager.stopUpdatingLocation()
            requestWeatherForLocation()
        }
        fetchCityAndCountry(from: currentLocation!) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            self.city = city
            self.country = country
            self.lblLocation.text! = self.getHour(Date(timeIntervalSince1970: TimeInterval(self.currently.time))).format() + ", " + self.city + ", " + self.country
        }
    }
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
        }
    }
    func requestWeatherForLocation() {
        showSpinner(onView: self.view)
        guard let currentLocation = currentLocation else {
            return
        }
        let lng = currentLocation.coordinate.longitude
        let lat = currentLocation.coordinate.latitude
        APIService.shared.GetFullAPI(lat.description, lng.description) { (current, listDay, timezone, error) in
            if let temp = current as? TodayModel {self.currently = temp}
            if let temp = timezone as? String {self.timezone = temp}
            if let temp = listDay as? [DailyModel] {
                self.daily = temp
                self.daily.remove(at: 0)
                self.lblCurrentTemputure.text = Int(self.currently.temperature.formatUnit()).description + "°"
                self.lblDate.text = self.getDate(Date(timeIntervalSince1970: TimeInterval(self.currently.time)))
                self.lblLocation.text = self.getHour(Date(timeIntervalSince1970: TimeInterval(self.currently.time))).format() + ", " + self.city + ", " + self.country
                if let image = UIImage(named: self.currently.icon) { self.imgIcon.image = image}
                self.lblSeason.text = self.currently.icon.replacingOccurrences(of: "-", with: " ")
                self.tblView.reloadData()
                self.removeSpinner()
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            self.removeSpinner()
        }
    }
    @objc func didSelectBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
extension WeatherController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if daily.count >= 6 {
            return 6
        }
        return daily.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! WeatherCell
        let item = daily[indexPath.row]
        let avg = (item.temperatureLow + item.temperatureHigh) / 2.0
        var day = getDayOfDate(Date(timeIntervalSince1970: Double(item.time)))
        day = day.uppercased()
        cell.lblT.text = String(Array(day)[0])
        cell.lblU.text = String(Array(day)[1])
        cell.lblE.text = String(Array(day)[2])
        cell.lblTempurature.text = Int(avg.formatUnit()).description + "°"
        cell.lblName.text = item.icon.replacingOccurrences(of: "-", with: " ")
        if let image = UIImage(named: item.icon){ cell.imgIcon.image = image}
        return cell
    }
    func getDayOfDate(_ date: Date?) -> String{
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        return formatter.string(from: inputDate)
    }
    func getDate(_ date: Date?) -> String{
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, d MMM YYYY"
        return formatter.string(from: inputDate)
    }
    func getHour(_ date: Date?) -> String{
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: inputDate)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70 * scale
    }
}

extension WeatherController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        vSpinner = spinnerView
    }
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}
