//
//  AvatarController.swift
//  DearCalls
//
//  Created by dovietduy on 12/2/20.
//

import UIKit
import StoreKit
import GoogleMobileAds
import MessageUI
class AvatarController: UIViewController {
    @IBOutlet weak var viewAboutUs: UIView!
    @IBOutlet weak var viewRateApp: UIView!
    @IBOutlet weak var viewShareApp: UIView!
    @IBOutlet weak var viewFeedBack: UIView!
    @IBOutlet weak var nativeAdView: GADUnifiedNativeAdView!
    @IBOutlet weak var heightViewAdmod: NSLayoutConstraint!
    var admobNativeAds: GADUnifiedNativeAd?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewAboutUs.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewAboutUs(_:))))
        viewShareApp.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewShareApp(_:))))
        viewRateApp.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewRateApp(_:))))
        viewFeedBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewFeedBack(_:))))
        heightViewAdmod.constant = 0
        if PaymentManager.shared.isPurchase(){
            
        }else{
            AdmobManager.shared.loadAllNativeAds()
            AdmobManager.shared.loadBannerView(inVC: self)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if PaymentManager.shared.isPurchase(){
            
        }else{
            if let native = AdmobManager.shared.randoomNativeAds(){
                admobNativeAds = native as? GADUnifiedNativeAd
                setupNativeAd(nativeAd: admobNativeAds!)
            }
        }
    }
    func setupNativeAd(nativeAd: GADUnifiedNativeAd) {
        heightViewAdmod.constant = 250 * scale
        nativeAdView.nativeAd = nativeAd
        
        (nativeAdView.headlineView as? UILabel)?.text = nativeAd.headline
        nativeAdView.mediaView?.mediaContent = nativeAd.mediaContent
        
        (nativeAdView.bodyView as? UILabel)?.text = nativeAd.body
        nativeAdView.bodyView?.isHidden = nativeAd.body == nil
        
        (nativeAdView.callToActionView as? UIButton)?.setTitle(nativeAd.callToAction, for: .normal)
        nativeAdView.callToActionView?.isHidden = nativeAd.callToAction == nil
        
        (nativeAdView.iconView as? UIImageView)?.image = nativeAd.icon?.image
        nativeAdView.iconView?.isHidden = nativeAd.icon == nil
        nativeAdView.callToActionView?.isUserInteractionEnabled = false
    }
    @objc func didSelectViewAboutUs(_ sender: Any){
        let vc = storyboard?.instantiateViewController(withIdentifier: AboutAppController.className) as! AboutAppController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    @objc func didSelectViewRateApp(_ sender: Any){
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else {
            if let url = URL(string: linkShareApp) {
                UIApplication.shared.open(url)
            }
        }
    }
    @objc func didSelectViewShareApp(_ sender: Any){
        if let linkapp = URL(string: linkShareApp){
            let itemsToShare = [linkapp]
            let ac = UIActivityViewController(activityItems: itemsToShare, applicationActivities: nil)
            ac.popoverPresentationController?.sourceView = self.view
            self.present(ac, animated: true)
            print("share oke")
        } else {
            print("Not found app in app store")
        }
    }
    @objc func didSelectViewFeedBack(_ sender: Any){
        if MFMailComposeViewController.canSendMail(){
            let composer = MFMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                composer.mailComposeDelegate = self
                composer.setToRecipients(["dovietduy97@gmail.com"])
                composer.setSubject("Feedback on Hunting Calls app")
                composer.setMessageBody("", isHTML: false)
                present(composer, animated: true, completion: nil)
            }
        }
    }
    
}
extension AvatarController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
        switch result {
        case .sent:
            print("did sent")
        default:
            print("eror sent")
        }
    }
}
