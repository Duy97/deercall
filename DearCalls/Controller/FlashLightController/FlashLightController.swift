//
//  FlashLightController.swift
//  DearCalls
//
//  Created by dovietduy on 12/2/20.
//

import UIKit
import AVFoundation

class FlashLightController: UIViewController {

    @IBOutlet weak var progressBar: ProgressBar!
    @IBOutlet weak var linearProgress: UISlider!
    @IBOutlet weak var imgBtnOn: UIImageView!
    var isOn = false
    override func viewDidLoad() {
        super.viewDidLoad()
        linearProgress.value = 0.0
        progressBar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapBtnOn(_ :))))
        if PaymentManager.shared.isPurchase(){
            
        }else{
            AdmobManager.shared.loadBannerView(inVC: self)
        }
    }
    @objc func didTapBtnOn(_ sender: UITapGestureRecognizer){
        isOn = !isOn
        if isOn{
            progressBar.progress = 0.2
            linearProgress.value = 0.2
            toggleTorch(on: true, level: 0.2)
        } else{
            progressBar.progress = 0.0
            linearProgress.value = 0.0
            toggleTorch(on: false, level: 0.0)
        }
    }
    @IBAction func valueChanged(_ sender: UISlider) {
        if sender.value == 0.0 {
            isOn = false
            toggleTorch(on: false, level: sender.value)
            progressBar.progress = CGFloat(sender.value)
        }else{
            progressBar.progress = CGFloat(sender.value)
            toggleTorch(on: true, level: sender.value)
            isOn = true
        }
    }
    func toggleTorch(on: Bool, level: Float) {
        guard let device = AVCaptureDevice.default(for: .video) else { return }

        if device.hasTorch {
            do {
                try device.lockForConfiguration()

                if on == true {
                    device.torchMode = .on
                    try device.setTorchModeOn(level: level)
                } else {
                    device.torchMode = .off
                }

                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
    }

}
