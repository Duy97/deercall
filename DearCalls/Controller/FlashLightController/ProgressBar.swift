//
//  ProgressBar.swift
//  demoFlashLight
//
//  Created by dovietduy on 12/20/20.
//

import UIKit
@IBDesignable
class ProgressBar: UIView {
    @IBInspectable public var backGroundCircleColor: UIColor = UIColor.lightGray
    @IBInspectable public var startGradientColor: UIColor = UIColor.red
    @IBInspectable public var endGradientColor: UIColor = UIColor.orange
    private var backgroundLayer: CAShapeLayer!
    private var foregroundLayer: CAShapeLayer!
    private var gradientlayer: CAGradientLayer!
    public var progress: CGFloat = 0{
        didSet{
            didProgressUpdate()
        }
    }
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        let width = rect.width
        let heìght = rect.height
        let lineWidth = 0.2 * min(width, heìght)
        backgroundLayer = createCicularLayer(rect: rect, strokeColor: backGroundCircleColor.cgColor, fillColor: UIColor.clear.cgColor, lineWidth: lineWidth)
        foregroundLayer = createCicularLayer(rect: rect, strokeColor: UIColor.blue.cgColor, fillColor: UIColor.clear.cgColor, lineWidth: lineWidth)
        foregroundLayer?.strokeEnd = 0.0
        gradientlayer = CAGradientLayer()
        gradientlayer.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradientlayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        gradientlayer.colors = [startGradientColor.cgColor, endGradientColor.cgColor]
        gradientlayer.frame = rect
        gradientlayer.mask = foregroundLayer
        layer.addSublayer(backgroundLayer)
        layer.addSublayer(gradientlayer)
    }
    private func createCicularLayer(rect: CGRect, strokeColor: CGColor,fillColor: CGColor, lineWidth: CGFloat ) -> CAShapeLayer{
        let width = rect.width
        let heìght = rect.height
        let lineWidth = 0.2 * min(width, heìght)
        let center = CGPoint(x: width / 2, y: heìght / 2)
        let radius = (min(width, heìght) - lineWidth) / 2
        let startAngle = -CGFloat.pi / 2
        let endAngle = startAngle + 2 * CGFloat.pi
        let cỉrcularPath = UIBezierPath(arcCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = cỉrcularPath.cgPath
        shapeLayer.strokeColor = strokeColor
        shapeLayer.fillColor = fillColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineCap = .round
        return shapeLayer
    }
    private func didProgressUpdate(){
        foregroundLayer?.strokeEnd = progress
    }
}
