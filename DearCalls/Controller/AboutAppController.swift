//
//  AboutAppController.swift
//  DearCalls
//
//  Created by dovietduy on 12/29/20.
//

import UIKit

class AboutAppController: UIViewController {

    @IBOutlet weak var viewBack: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
    }
    @objc func didSelectBtnBack(_ sender: Any){
        dismiss(animated: true, completion: nil)
    }
}
