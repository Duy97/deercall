//
//  DailyController.swift
//  DearCalls
//
//  Created by dovietduy on 12/25/20.
//

import UIKit
import CoreLocation
let now = Date()
var lat = 0.0
var lng = 0.0
var timeZone = ""

class DailyController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMajor1: UILabel!
    @IBOutlet weak var lblMajor2: UILabel!
    @IBOutlet weak var lblMinor1: UILabel!
    @IBOutlet weak var lblMinor2: UILabel!
    @IBOutlet weak var lblIluminationRate: UILabel!
    @IBOutlet weak var lblSunRise: UILabel!
    @IBOutlet weak var lblSunSet: UILabel!
    @IBOutlet weak var lblMoonRise: UILabel!
    @IBOutlet weak var lblMoonOverHead: UILabel!
    @IBOutlet weak var lblMoonSet: UILabel!
    @IBOutlet weak var lblMoonUnder: UILabel!
    @IBOutlet weak var viewContent: UIImageView!
    @IBOutlet var viewContainer: UIView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var imgMoon: UIImageView!
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var index = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: Notification.Name("setup"), object: nil)
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        lblDay.text = getDate(now)
        let swipeLeft = UISwipeGestureRecognizer()
        let swipeRight = UISwipeGestureRecognizer()
        swipeLeft.direction = .left
        swipeRight.direction = .right
        viewContainer.addGestureRecognizer(swipeLeft)
        viewContainer.addGestureRecognizer(swipeRight)
        swipeLeft.addTarget(self, action: #selector(didSwipeLeft(_:)))
        swipeRight.addTarget(self, action: #selector(didSwipeRight(_:)))
        currentDate = now
        setupLocation()
    }
    var currentDate: Date!
    @objc func refresh(){
        let day = getDateForAPI(currentDate)
        setup(lat: lat.description, lng: lng.description, day: day, timeZone: timeZone)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @IBAction func didSelectBtnSetting(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SolunarSettingController") as! SolunarSettingController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    func setup(lat: String, lng: String, day: String, timeZone: String){
        viewContent.show_Spinner()
        APIService.shared.GetSoluarAPI(lat, lng, day, timeZone){ (data, error) in
            if let data = data as? SolunarModel{
                self.lblMajor1.text = data.major1Start.format() + " until " + data.major1Stop.format()
                self.lblMajor2.text = data.major2Start.format() + " until " + data.major2Stop.format()
                self.lblMinor1.text = data.minor1Start.format() + " until " + data.minor1Stop.format()
                self.lblMinor2.text = data.minor2Start.format() + " until " + data.minor2Stop.format()
                self.lblSunRise.text = "Rise: " + data.sunRise.format()
                self.lblSunSet.text = "Set: " + data.sunSet.format()
                self.lblMoonRise.text = "Rise: " + data.moonRise.format()
                self.lblMoonOverHead.text = "Overhead: " + data.moonTransit.format()
                self.lblMoonSet.text = "Set: " + data.moonSet.format()
                self.lblMoonUnder.text = "Underfoot: " + data.moonUnder.format()
                self.lblIluminationRate.text = Int(data.moonIllumination * 100).description + "%"
                self.setIllumination(value: Int(data.moonIllumination * 100))
                self.progressBar.setRating(data.dayRating)
                self.viewContent.remove_Spinner()
            }
        }
    }
    func setIllumination(value: Int){
            switch value {
            case 0...10:
                imgMoon.image = UIImage(named: "moon_0")
            case 11...39:
                imgMoon.image = UIImage(named: "moon_25")
            case 40...69:
                imgMoon.image = UIImage(named: "moon_50")
            case 70...89:
                imgMoon.image = UIImage(named: "moon_75")
            case 90...100:
                imgMoon.image = UIImage(named: "moon_100")
            default:
                break
            }
        
    }

    @objc func didSwipeLeft(_ sender: UISwipeGestureRecognizer){
        index += 1
        let date = Calendar.current.date(byAdding: .day, value: index, to: now)
        let day = getDateForAPI(date)
        currentDate = date
        setup(lat: lat.description, lng: lng.description, day: day, timeZone: timeZone)
        lblDay.text = getDate(date)
    }
    @objc func didSwipeRight(_ sender: UISwipeGestureRecognizer){
        index -= 1
        let date = Calendar.current.date(byAdding: .day, value: index, to: now)
        currentDate = date
        let day = getDateForAPI(date)
        setup(lat: lat.description, lng: lng.description, day: day, timeZone: timeZone)
        lblDay.text = getDate(date)
    }
    func getDate(_ date: Date?) -> String{
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, d MMM YYYY"
        return formatter.string(from: inputDate)
    }
    func getDateForAPI(_ date: Date?) -> String{
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYYMMdd"
        return formatter.string(from: inputDate)
    }
    func getTimeZone() -> String{
        var localTimeZoneAbbreviation: String { return TimeZone.current.abbreviation() ?? "" }
        let strs = localTimeZoneAbbreviation.split(separator: "+")
        if strs.count == 2{
            return String(strs[1])
        }
        let str2s = localTimeZoneAbbreviation.split(separator: "-")
        if str2s.count == 2 {
            return "-" + str2s[1]
        }
        return ""
    }
    @objc func didSelectBtnBack(_ sender: UIButton){
        dismiss(animated: true, completion: nil)
    }
    func setupLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !locations.isEmpty, currentLocation == nil  {
            currentLocation = locations.first
            locationManager.stopUpdatingLocation()
            guard let currentLocation = currentLocation else {return}
            lng = currentLocation.coordinate.longitude
            lat = currentLocation.coordinate.latitude
            timeZone = getTimeZone()
            let day = getDateForAPI(now)
            setup(lat: lat.description, lng: lng.description, day: day, timeZone: timeZone)
        }
    }
}

extension UIProgressView{
    func setRating(_ rate: Int){
        switch rate {
        case 0:
            self.setProgress(0.01, animated: true)
            self.progressImage = #imageLiteral(resourceName: "1_progress")
            self.trackImage = #imageLiteral(resourceName: "0")
            break
        case 1:
            self.setProgress(0.2, animated: true)
            self.progressImage = #imageLiteral(resourceName: "1_progress")
            self.trackImage = #imageLiteral(resourceName: "1")
            break
        case 2:
            self.setProgress(0.4, animated: true)
            self.progressImage = #imageLiteral(resourceName: "2_progress")
            self.trackImage = #imageLiteral(resourceName: "0")
            break
        case 3:
            self.setProgress(0.6, animated: true)
            self.progressImage = #imageLiteral(resourceName: "3")
            break
        case 4:
            self.setProgress(0.8, animated: true)
            self.progressImage = #imageLiteral(resourceName: "4")
            break
        case 5:
            self.setProgress(1.0, animated: true)
            self.progressImage = #imageLiteral(resourceName: "5")
            break
        default:
            break
        }
    }
}
