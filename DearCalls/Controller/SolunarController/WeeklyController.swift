//
//  WeeklyController.swift
//  DearCalls
//
//  Created by dovietduy on 12/7/20.
//

import UIKit

class WeeklyController: UIViewController {

    @IBOutlet var viewContainer: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewBack: UIView!
    var listData: [SolunarModel] = []
    var index = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: Notification.Name("setup"), object: nil)
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        tblView.dataSource = self
        tblView.delegate = self
        tblView.register(UINib(nibName: "WeeklyCell", bundle: nil), forCellReuseIdentifier: "WeeklyCell")
        let swipeLeft = UISwipeGestureRecognizer()
        let swipeRight = UISwipeGestureRecognizer()
        swipeLeft.direction = .left
        swipeRight.direction = .right
        viewContainer.addGestureRecognizer(swipeLeft)
        viewContainer.addGestureRecognizer(swipeRight)
        swipeLeft.addTarget(self, action: #selector(didSwipeLeft(_:)))
        swipeRight.addTarget(self, action: #selector(didSwipeRight(_:)))
        setup()
    }
    @objc func refresh(){
        tblView.reloadData()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @IBAction func didSelectBtnSetting(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SolunarSettingController") as! SolunarSettingController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    @objc func didSwipeLeft(_ sender: UISwipeGestureRecognizer){
        index += 7
        setup()
    }
    @objc func didSwipeRight(_ sender: UISwipeGestureRecognizer){
        index -= 7
        setup()
    }
    func setup(){
        listData = []
        for i in (0...6) {
            let nextDay = Calendar.current.date(byAdding: .day, value: index + i, to: now)
            APIService.shared.GetSoluarAPI(lat.description, lng.description, getDateForAPI(nextDay), timeZone){ (data, error) in
                if let data = data as? SolunarModel{
                    data.day = self.getDate(nextDay)
                    data.index = i
                    self.listData.append(data)
                    self.listData.sort{ $0.index < $1.index}
                    self.tblView.reloadData()
                }
            }
        }
        
    }
    func getDate(_ date: Date?) -> String{
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, d MMM YYYY"
        return formatter.string(from: inputDate)
    }
    func getDateForAPI(_ date: Date?) -> String{
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYYMMdd"
        return formatter.string(from: inputDate)
    }
    @objc func didSelectBtnBack(_ sender: UIButton){
        dismiss(animated: true, completion: nil)
    }
}
extension WeeklyController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeeklyCell", for: indexPath) as! WeeklyCell
        if indexPath.row < listData.count{
            let data = listData[indexPath.row]
            cell.lblDay.text = data.day
            
            cell.lblMajor1.text = data.major1Start.format() + " until " + data.major1Stop.format()
            cell.lblMajor2.text = data.major2Start.format() + " until " + data.major2Stop.format()
            cell.lblMinor1.text = data.minor1Start.format() + " until " + data.minor1Stop.format()
            cell.lblMinor2.text = data.minor2Start.format() + " until " + data.minor2Stop.format()
            cell.progressBar.setRating(data.dayRating)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 260 * scale
    }
}

