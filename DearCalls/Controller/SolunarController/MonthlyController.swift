//
//  MonthlyController.swift
//  DearCalls
//
//  Created by dovietduy on 12/26/20.
//

import UIKit
import FSCalendar

class MonthlyController: UIViewController, FSCalendarDelegate {

    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMajor1: UILabel!
    @IBOutlet weak var lblMajor2: UILabel!
    @IBOutlet weak var lblMinor1: UILabel!
    @IBOutlet weak var lblMinor2: UILabel!
    @IBOutlet weak var calenar: FSCalendar!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var viewBack: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: Notification.Name("setup"), object: nil)
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        calenar.delegate = self
        lblDay.text = getDate(calenar.today)
        setup(lat: lat.description, lng: lng.description, day: getDateForAPI(calenar.today), timeZone: timeZone)
    }
    @objc func refresh(){
        setup(lat: lat.description, lng: lng.description, day: getDateForAPI(calenar.today), timeZone: timeZone)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @IBAction func didSelectBtnSetting(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SolunarSettingController") as! SolunarSettingController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    func setup(lat: String, lng: String, day: String, timeZone: String){
        imgView.show_Spinner()
        APIService.shared.GetSoluarAPI(lat, lng, day, timeZone){ (data, error) in
            if let data = data as? SolunarModel{
                self.lblMajor1.text = data.major1Start.format() + " until " + data.major1Stop.format()
                self.lblMajor2.text = data.major2Start.format() + " until " + data.major2Stop.format()
                self.lblMinor1.text = data.minor1Start.format() + " until " + data.minor1Stop.format()
                self.lblMinor2.text = data.minor2Start.format() + " until " + data.minor2Stop.format()
                self.progressBar.setRating(data.dayRating)
                self.imgView.remove_Spinner()
            }
        }
    }
    func getDate(_ date: Date?) -> String{
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, d MMM YYYY"
        return formatter.string(from: inputDate)
    }
    func getDateForAPI(_ date: Date?) -> String{
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYYMMdd"
        return formatter.string(from: inputDate)
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        lblDay.text = getDate(date)
        setup(lat: lat.description, lng: lng.description, day: getDateForAPI(date), timeZone: timeZone)
    }
    @objc func didSelectBtnBack(_ sender: Any){
        dismiss(animated: true, completion: nil)
    }
}
