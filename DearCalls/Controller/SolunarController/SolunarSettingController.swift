//
//  SolunarSettingController.swift
//  DearCalls
//
//  Created by dovietduy on 12/28/20.
//

import UIKit

class SolunarSettingController: UIViewController {

    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var btn12hr: UIButton!
    @IBOutlet weak var btn24hr: UIButton!
    @IBOutlet weak var btnF: UIButton!
    @IBOutlet weak var btnC: UIButton!
    @IBOutlet weak var viewBack: UIView!
    var onComplete: (() -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectBtnBack(_:))))
        let is12 = UserDefaults.standard.bool(forKey: "is12")
        if is12 == true {
            setup12h()
        } else{
            setup24h()
        }
        let isF = UserDefaults.standard.bool(forKey: "isF")
        if isF == true {
            setupF()
        } else{
            setupC()
        }
    }
    @objc func didSelectBtnBack(_ sender: Any){
        onComplete?()
        dismiss(animated: true, completion: nil)
    }
    @IBAction func didSelect12hr(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("setup"), object: nil)
        UserDefaults.standard.setValue(true, forKey: "is12")
        setup12h()
    }
    @IBAction func didSelect24hr(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("setup"), object: nil)
        UserDefaults.standard.setValue(false, forKey: "is12")
        setup24h()
    }
    @IBAction func didSelectF(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("setup"), object: nil)
        UserDefaults.standard.setValue(true, forKey: "isF")
        setupF()
    }
    @IBAction func didSelectC(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("setup"), object: nil)
        UserDefaults.standard.setValue(false, forKey: "isF")
        setupC()
    }
    @IBAction func ValueChanged(_ sender: UISlider) {
        UIScreen.main.brightness = CGFloat(sender.value)
    }
    func setup12h(){
        btn12hr.backgroundColor = .blue
        btn12hr.setTitleColor(.white, for: .normal)
        btn24hr.backgroundColor = .white
        btn24hr.setTitleColor(.black, for: .normal)
    }
    func setup24h(){
        btn24hr.backgroundColor = .blue
        btn24hr.setTitleColor(.white, for: .normal)
        btn12hr.backgroundColor = .white
        btn12hr.setTitleColor(.black, for: .normal)
    }
    
    func setupC(){
        btnC.backgroundColor = .blue
        btnC.setTitleColor(.white, for: .normal)
        btnF.backgroundColor = .white
        btnF.setTitleColor(.black, for: .normal)
    }
    func setupF(){
        btnF.backgroundColor = .blue
        btnF.setTitleColor(.white, for: .normal)
        btnC.backgroundColor = .white
        btnC.setTitleColor(.black, for: .normal)
    }
}
