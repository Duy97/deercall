//
//  TabBarSolunarController.swift
//  DearCalls
//
//  Created by dovietduy on 12/21/20.
//

import UIKit

class TabBarSolunarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        self.selectedIndex = 0
    }

}
