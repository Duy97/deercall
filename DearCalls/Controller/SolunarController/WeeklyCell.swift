//
//  WeeklyCell.swift
//  DearCalls
//
//  Created by dovietduy on 12/7/20.
//

import UIKit

class WeeklyCell: UITableViewCell {

    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMajor1: UILabel!
    @IBOutlet weak var lblMajor2: UILabel!
    @IBOutlet weak var lblMinor1: UILabel!
    @IBOutlet weak var lblMinor2: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
