//
//  PopUpRepeatController.swift
//  DearCalls
//
//  Created by dovietduy on 12/15/20.
//

import UIKit

class PopUpRepeatController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var pickerMinute: UIPickerView!
    @IBOutlet weak var pickerSecond: UIPickerView!
    @IBOutlet weak var viewCancel: UIView!
    @IBOutlet weak var viewDone: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var txfDownPicker: UITextField!
    @IBOutlet weak var lblMinutes: UILabel!
    @IBOutlet weak var lblSeconds: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var leadingPickerMinutes: NSLayoutConstraint!
    @IBOutlet weak var viewBack: UIView!
    
    var didSendData: ((String, String, String, String) -> Void)?
    var strMinutes = ""
    var strSeconds = ""
    let listPicker = ["Infinite", "Finite", "Timed"]
    let listIcon = ["ic_infinite", "ic_refresh", "ic_timed"]
    var strType = "Infinite"
    var unit = "mins"
    var icon = "ic_infinite"
    var picker = UIPickerView()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewCancel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectCancel(_ :))))
        viewDone.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectDone(_ :))))
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewBack(_ :))))
        pickerMinute.delegate = self
        pickerMinute.dataSource = self
        pickerSecond.delegate = self
        pickerSecond.dataSource = self
        picker.delegate = self
        picker.dataSource = self
        pickerSecond.tag = 2
        pickerMinute.tag = 1
        picker.tag = 3
        txfDownPicker.inputView = picker
        switch strType {
        case "Infinite":
            pickerMinute.selectRow(Int(strMinutes) ?? 0, inComponent: 0, animated: true)
            pickerSecond.selectRow(Int(strSeconds) ?? 0, inComponent: 0, animated: true)
            picker.selectRow(0, inComponent: 0, animated: true)
            txfDownPicker.text = "Infinite"
            imgIcon.image = UIImage(named: listIcon[0])
            setup(0)
        case "Finite":
            pickerMinute.selectRow(Int(strMinutes) ?? 0, inComponent: 0, animated: true)
            picker.selectRow(1, inComponent: 0, animated: true)
            txfDownPicker.text = "Finite"
            imgIcon.image = UIImage(named: listIcon[1])
            setup(1)
        case "Timed":
            pickerMinute.selectRow(Int(strMinutes) ?? 0, inComponent: 0, animated: true)
            pickerSecond.selectRow(Int(strSeconds) ?? 0, inComponent: 0, animated: true)
            picker.selectRow(2, inComponent: 0, animated: true)
            txfDownPicker.text = "Timed"
            imgIcon.image = UIImage(named: listIcon[2])
            setup(2)
        default:
            print("not found")
        }
    }
    @objc func didSelectCancel(_ sender: UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
        
    }
    @objc func didSelectDone(_ sender: UITapGestureRecognizer){
        strMinutes = setupTime(row: pickerMinute.selectedRow(inComponent: 0))
        strSeconds = setupTime(row: pickerSecond.selectedRow(inComponent: 0))
        didSendData?(strMinutes + "." + strSeconds, (strType == "") ? "Infinite" : strType, unit, icon)
        dismiss(animated: true, completion: nil)
    }
    @objc func didSelectViewBack(_ sender: UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 3 {
            return 3
        }
        return 60
    }
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        if pickerView.tag == 3 {
//            return listPicker[row]
//        } else {
//            if row < 10 {
//                return "0\(row)"
//            }
//            return row.description
//        }
//
//    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont(name: "SanFranciscoText-Light", size: 18 * scale)
        if pickerView.tag == 3 {
            label.text = listPicker[row]
        } else {
            if row < 10 {
                label.text = "0\(row)"
            }
            label.text = row.description
        }
        return label
    }
    func setupTime(row: Int) -> String {
        if row < 10{
            return "0\(row)"
        } else{
            return "\(row)"
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 3{
            txfDownPicker.text = listPicker[row]
            imgIcon.image = UIImage(named: listIcon[row])
            icon = listIcon[row]
            strType = listPicker[row]
            txfDownPicker.resignFirstResponder()
            setup(row)
        }
    }
    func setup(_ row: Int){
        if row == 1 {
            pickerSecond.isHidden = true
            lblSeconds.isHidden = true
            lblMinutes.text = "times"
            unit = "times"
            lblNote.text = "This will play the call the specified number of times with no pause in between. Example: if set to 5 times, the call will repeat 5 times and then stop."
        } else if row == 0 {
            lblNote.text = "This will repeat the call infinitely at the specified interval . Example : if set to 2 mins , the call will repeat every 2:00 minutes forever ."
            pickerSecond.isHidden = false
            lblSeconds.isHidden = false
            lblMinutes.text = "minutes"
            unit = "mins"
        } else if row == 2 {
            lblNote.text = "This will play the call for the specified time length with no pause in between. Example: if set to 3 mins, the call will repeat until 3 mins have passed."
            pickerSecond.isHidden = false
            lblSeconds.isHidden = false
            lblMinutes.text = "minutes"
            unit = "mins"
        }
    }
}
