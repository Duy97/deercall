//
//  PopUpVolumeController.swift
//  DearCalls
//
//  Created by dovietduy on 12/15/20.
//

import UIKit

class PopUpVolumeController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var viewCancel: UIView!
    @IBOutlet weak var viewDone: UIView!
    @IBOutlet weak var viewBack: UIView!
    var volume = 0
    var didSendData: ((Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewCancel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectCancel(_ :))))
        viewDone.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectDone(_ :))))
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectviewBack(_ :))))
        picker.delegate = self
        picker.dataSource = self
        picker.selectRow(volume, inComponent: 0, animated: true)
    }
    @objc func didSelectCancel(_ sender: UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
    }
    @objc func didSelectDone(_ sender: UITapGestureRecognizer){
        didSendData?(picker.selectedRow(inComponent: 0))
        dismiss(animated: true, completion: nil)
    }
    @objc func didSelectviewBack(_ sender: UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 101
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
       let label = (view as? UILabel) ?? UILabel()

       label.textColor = .black
       label.textAlignment = .center
       label.font = UIFont(name: "SanFranciscoText-Light", size: 18 * scale)
    
       label.text = row.description

       return label
     }
}
