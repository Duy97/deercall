//
//  PopUpController.swift
//  DearCalls
//
//  Created by dovietduy on 12/15/20.
//

import UIKit

class PopUpController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var pickerMinute: UIPickerView!
    @IBOutlet weak var pickerSecond: UIPickerView!
    @IBOutlet weak var viewCancel: UIView!
    @IBOutlet weak var viewDone: UIView!
    @IBOutlet weak var viewBack: UIView!
    
    var didSendData: ((String) -> Void)?
    var strMinutes = "00"
    var strSeconds = "00"
    override func viewDidLoad() {
        super.viewDidLoad()
        viewCancel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectCancel(_ :))))
        viewDone.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectDone(_ :))))
        viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelectViewBack(_ :))))
        pickerMinute.delegate = self
        pickerMinute.dataSource = self
        pickerSecond.delegate = self
        pickerSecond.dataSource = self
        pickerMinute.selectRow(Int(strMinutes)!, inComponent: 0, animated: true)
        pickerSecond.selectRow(Int(strSeconds)!, inComponent: 0, animated: true)
    }
    @objc func didSelectCancel(_ sender: UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
    }
    @objc func didSelectDone(_ sender: UITapGestureRecognizer){
        strMinutes = setupTime(row: pickerMinute.selectedRow(inComponent: 0))
        strSeconds = setupTime(row: pickerSecond.selectedRow(inComponent: 0))
        didSendData?(strMinutes + "." + strSeconds)
        dismiss(animated: true, completion: nil)
    }
    @objc func didSelectViewBack(_ sender: UITapGestureRecognizer){
        dismiss(animated: true, completion: nil)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.font = UIFont(name: "SanFranciscoText-Light", size: 18 * scale)
        if row < 10 {
            label.text = "0\(row)"
        }
        label.text = row.description
        return label
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 60
    }
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        if row < 10 {
//            return "0\(row)"
//        }
//        return row.description
//    }
    func setupTime(row: Int) -> String {
        if row < 10{
            return "0\(row)"
        } else{
            return "\(row)"
        }
    }
}
