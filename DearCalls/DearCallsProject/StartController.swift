//
//  StartController.swift
//  DearCalls
//
//  Created by dovietduy on 12/2/20.
//

import UIKit

class StartController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if PaymentManager.shared.isPurchase(){
            
        }else{
            AdmobManager.shared.loadBannerView(inVC: self)
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func didSelectBtnLetsgo(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: TabBarHomeController.className) as! TabBarHomeController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if PaymentManager.shared.isPurchase(){
            
        }else{
            AdmobManager.shared.loadAdFull(inVC: self)
        }
    }
}
