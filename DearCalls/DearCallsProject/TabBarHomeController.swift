//
//  TabBarHomeController.swift
//  DearCalls
//
//  Created by dovietduy on 12/3/20.
//

import UIKit

class TabBarHomeController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
        }
        self.selectedIndex = 1
    }
}


class CustomTabBar : UITabBar {
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = 100 * scale
        return sizeThatFits
    }
}
