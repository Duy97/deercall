//
//  AudioPlayer.swift
//  DearCalls
//
//  Created by dovietduy on 12/9/20.
//

import Foundation
import AVFoundation

class AudioPlayer: NSObject, AVAudioPlayerDelegate{
    var player : AVAudioPlayer!
    var delegate: AudioPlayerDelegate!
    var onDelaying: ((String) -> Void)?
    var onTimed: ((String) -> Void)?
    var onInfinite: ((Bool) -> Void)?
    override init() {
    }
    func playAudio(fileURL: URL) {
        do {
            self.player = try AVAudioPlayer(contentsOf: fileURL)
            player.delegate = self
            player.volume = 1.0
            player.play()
        } catch let error as NSError {
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
        
    }
//    func playAudio(fileURL: URL,_ setup: SettingModel) {
//        do {
//            self.player = try AVAudioPlayer(contentsOf: fileURL)
//            player.delegate = self
//            player.volume = Float(setup.volume) / 100.0
//            if setup.isDelay == true {
//                let strs = setup.delayDuration.split(separator: ".")
//                let time = Double(strs[0])! * 60 + Double(strs[1])!
//                onDelaying?("delay \(time) mins")
//                DispatchQueue.main.asyncAfter(deadline: .now() + time) {
//                    self.player.play()
//                }
//            }
//            if setup.isRepeat == true {
//                switch setup.repeatType {
//                case "Infinite":
//                    let strs = setup.repeatDuration.split(separator: ".")
//                    let time = Double(strs[0])! * 60 + Double(strs[1])!
//                    player.play()
//                    Timer.scheduledTimer(withTimeInterval: time, repeats: true) { (timer) in
//                        self.player.play()
//                    }
//                case "Finite":
//                    player.numberOfLoops = Int(setup.repeatDuration)!
//                    player.play()
//                    print()
//                case "Timed":
//                    print()
//                    let strs = setup.repeatDuration.split(separator: ".")
//                    let timeOut = Double(strs[0])! * 60 + Double(strs[1])!
//                    let numloop =  timeOut / player!.duration
//                    if numloop > 1 {
//                        player.numberOfLoops = Int(numloop)
//                        player.play()
//                        DispatchQueue.main.asyncAfter(deadline: .now() + timeOut) {
//                            self.onTimed?("Time out")
//                        }
//                    } else{
//                        player.play()
//                        DispatchQueue.main.asyncAfter(deadline: .now() + timeOut) {
//                            self.onTimed?("Time out")
//                        }
//                    }
//                default:
//                    print("not repeat")
//                }
//            } else{
//                player.play()
//            }
//
//        } catch let error as NSError {
//            print(error.localizedDescription)
//        } catch {
//            print("AVAudioPlayer init failed")
//        }
//
//    }
    func playAudio(fileURL: URL,_ setup: SettingModel) {
        do {
            self.player = try AVAudioPlayer(contentsOf: fileURL)
            player.delegate = self
            player.volume = Float(setup.volume) / 100.0
            switch (setup.isDelay, setup.isRepeat) {
            case (true, true):
                print(setup.delayDuration)
                var strs: [Substring] = []
                if setup.delayDuration.contains(".") {
                    strs = setup.delayDuration.split(separator: ".")
                }
                if setup.delayDuration.contains(",") {
                    strs = setup.delayDuration.split(separator: ",")
                }
                let time = Double(strs[0])! * 60 + Double(strs[1])!
                onDelaying?("delay \(time) secs")
                DispatchQueue.main.asyncAfter(deadline: .now() + time) {
                    self.repeatAction(setup)
                }
            case (true, false):
                var strs: [Substring] = []
                if setup.delayDuration.contains(".") {
                    strs = setup.delayDuration.split(separator: ".")
                }
                if setup.delayDuration.contains(",") {
                    strs = setup.delayDuration.split(separator: ",")
                }
                let time = Double(strs[0])! * 60 + Double(strs[1])!
                onDelaying?("delay \(time) secs")
                DispatchQueue.main.asyncAfter(deadline: .now() + time) {
                    self.player.play()
                }
            case (false, true):
                repeatAction(setup)
            case (false, false):
                self.player.play()
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
        
    }
    func repeatAction(_ setup: SettingModel){
        switch setup.repeatType {
        case "Infinite":
            var strs: [Substring] = []
            if setup.delayDuration.contains(".") {
                strs = setup.delayDuration.split(separator: ".")
            }
            if setup.delayDuration.contains(",") {
                strs = setup.delayDuration.split(separator: ",")
            }
            let timeOut = Double(strs[0])! * 60 + Double(strs[1])!
            if player != nil  {
                player.play()
            }
            
            onInfinite?(true)
            if let player = player {
                Timer.scheduledTimer(withTimeInterval: player.duration + timeOut, repeats: true) { (timer) in
                    self.playIfCan()
                }
            }
            
        case "Finite":
            onInfinite?(false)
            player.numberOfLoops = Int(setup.repeatDuration)!
            player.play()
            print()
        case "Timed":
            onInfinite?(false)
            var strs: [Substring] = []
            if setup.delayDuration.contains(".") {
                strs = setup.delayDuration.split(separator: ".")
            }
            if setup.delayDuration.contains(",") {
                strs = setup.delayDuration.split(separator: ",")
            }
            let timeOut = Double(strs[0])! * 60 + Double(strs[1])!
            let numloop =  timeOut / player!.duration
            if numloop > 1 {
                player.numberOfLoops = Int(numloop)
                player.play()
                DispatchQueue.main.asyncAfter(deadline: .now() + timeOut) {
                    self.onTimed?("Time out")
                }
            } else{
                player.play()
                DispatchQueue.main.asyncAfter(deadline: .now() + timeOut) {
                    self.onTimed?("Time out")
                }
            }
        default:
            print("not repeat")
        }
    }
    func playIfCan(){
        if player != nil{
            player.play()
        }
    }
    func stopAudio(){
        if player != nil {
            player.stop()
            player = nil
        }
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        delegate?.didFinishPlaying()
    }
}

protocol AudioPlayerDelegate: class {
    func didFinishPlaying()
}
