//
//  GSAudio.swift
//  DearCalls
//
//  Created by dovietduy on 12/11/20.
//

import Foundation
import AVFoundation
class GSAudio: NSObject, AVAudioPlayerDelegate {
    override init() {}

    var players = [URL:AVAudioPlayer]()
    var duplicatePlayers = [AVAudioPlayer]()

    func playSound (soundFileNameURL: URL){

        //let soundFileNameURL = //NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource(soundFileName, ofType: "aif", inDirectory:"Sounds")!)

        if let player = players[soundFileNameURL] { //player for sound has been found

            if player.isPlaying == false { //player is not in use, so use that one
                player.prepareToPlay()
                player.play()

            } else { // player is in use, create a new, duplicate, player and use that instead

                let duplicatePlayer = try! AVAudioPlayer(contentsOf: soundFileNameURL)
                //use 'try!' because we know the URL worked before.

                duplicatePlayer.delegate = self
                //assign delegate for duplicatePlayer so delegate can remove the duplicate once it's stopped playing

                duplicatePlayers.append(duplicatePlayer)
                //add duplicate to array so it doesn't get removed from memory before finishing

                duplicatePlayer.prepareToPlay()
                duplicatePlayer.play()

            }
        } else { //player has not been found, create a new player with the URL if possible
            do{
                let player = try AVAudioPlayer(contentsOf: soundFileNameURL)
                players[soundFileNameURL] = player
                player.prepareToPlay()
                player.play()
            } catch {
                print("Could not play sound file!")
            }
        }
    }


    func playSounds(soundFileNames: [URL]){

        for soundFileName in soundFileNames {
            playSound(soundFileNameURL: soundFileName)
        }
    }

    func playSounds(soundFileNames: URL...){
        for soundFileName in soundFileNames {
            playSound(soundFileNameURL: soundFileName)
        }
    }
     func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        duplicatePlayers.remove(at: duplicatePlayers.firstIndex(of: player)!)
        //Remove the duplicate player once it is done
    }
    func stopAll(){
        players.removeAll()
        duplicatePlayers.removeAll()
    }
}
