//
//  GithubService.swift
//  DearCalls
//
//  Created by dovietduy on 12/9/20.
//

import Foundation
class GithubService: NSObject {
    var delegate: GithubServiceDelegate!
    
    func getData(fileName: String){
        let url = SDFileUtils.cacheDirectoryPath()
        let fileUrl = url.appendingPathComponent("Audios").appendingPathComponent(fileName)
        if FileManager.default.fileExists(atPath: fileUrl.path){
            self.delegate?.didDownloadFile(fileURL: fileUrl)
        } else {
            let url = URL(string: "https://github.com/dovietduy200397/CallsData/raw/main/" + fileName)
            let request = URLRequest(url: url!)
            SDDownloadManager.shared.downloadFile(withRequest: request, inDirectory: "Audios", shouldDownloadInBackground: true, onProgress: { progress in
                let percentage = String(format: "%.1f %", (progress * 100))
                debugPrint("Background progress : \(percentage)")
            }){ (error , url) in
                if let error = error{
                    print(error)
                }else{
                    if let url = url {
                        self.delegate?.didDownloadFile(fileURL: url)
                        print("vua tai ve thoi")
                    }
                }
                
            }
        }
        
    }
    func getData(fileNames: [String]){
        var fileURLs = [URL]()
        for fileName in fileNames{
            let url = SDFileUtils.cacheDirectoryPath()
            let fileUrl = url.appendingPathComponent("Audios").appendingPathComponent(fileName)
            if FileManager.default.fileExists(atPath: fileUrl.path){
                fileURLs.append(fileUrl)
            } else {
                let url = URL(string: "https://github.com/dovietduy200397/CallsData/raw/main/" + fileName)
                let request = URLRequest(url: url!)
                SDDownloadManager.shared.downloadFile(withRequest: request, inDirectory: "Audios", shouldDownloadInBackground: true, onProgress: { progress in
                    let percentage = String(format: "%.1f %", (progress * 100))
                    debugPrint("Background progress : \(percentage)")
                }){ (error , url) in
                    if let error = error{
                        print(error)
                    }else{
                        if let url = url {
                            fileURLs.append(url)
                            print("vua tai ve thoi")
                        }
                    }
                    
                }
            }
            self.delegate.didDownloadFiles(fileURLs: fileURLs)
        }
    }
}
protocol GithubServiceDelegate: class {
    func didDownloadFile(fileURL: URL)
    func didDownloadFiles(fileURLs: [URL])
}
