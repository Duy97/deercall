//
//  AnimalModel.swift
//  DearCalls
//
//  Created by dovietduy on 12/8/20.
//

import Foundation

class AnimalModel: NSObject{
    var idRes: Int = 0
    var image: String = ""
    var animalName: String = ""
    var Description: String = ""
    var mSounds: [SoundModel] = []

    func initLoad(_ json:  [String: Any]) -> AnimalModel{
        if let temp = json["idRes"] as? Int { idRes = temp }
        if let temp = json["image"] as? String { image = temp }
        if let temp = json["animalName"] as? String { animalName = temp }
        if let temp = json["Description"] as? String { Description = temp }
        if let temp = json["mSounds"] as? [[String: Any]] {
            for item in temp{
                var soundAdd = SoundModel()
                soundAdd = soundAdd.initLoad(item)
                mSounds.append(soundAdd)
            }
        }
        return self
    }
}

class SoundModel: NSObject, Codable{
    var uniqueID: Int = 0
    var timeLength: Double = 0.0
    var soundFile: String = ""
    var Description: String = ""
    var setting = SettingModel()

    func initLoad(_ json:  [String: Any]) -> SoundModel{
        if let temp = json["uniqueID"] as? Int { uniqueID = temp }
        if let temp = json["timeLength"] as? Double { timeLength = temp }
        if let temp = json["soundFile"] as? String { soundFile = temp }
        if let temp = json["Description"] as? String { Description = temp }
        if let temp = json["setting"] as? [String: Any] {
            let settingAdd = SettingModel()
            setting = settingAdd.initLoad(temp)
            
        }
        return self
    }
    func initLoad(uniqueID: Int,timeLength: Double,soundFile: String,Description: String){
        self.uniqueID = uniqueID
        self.timeLength = timeLength
        self.soundFile = soundFile
        self.Description = Description
    }
    func timeFormat() -> String{
        let min = Int(timeLength)/60
        let sec = Int(timeLength) % 60
        let minstr = String(format: "%02d", min)
        let sectr = String(format: "%02d", sec)
        if minstr == "00" && sectr == "00" {
            return "00.01"
        }
        return minstr + ":" + sectr
    }
}
class SettingModel: NSObject, Codable{
    var isDelay = false
    var delayDuration = ""
    var isRepeat = false
    var repeatType = "Infinite"
    var icon = ""
    var repeatDuration = ""
    var volume = 100
    override init() {}
    func initLoad(_ json:  [String: Any]) -> SettingModel{
        if let temp = json["isDelay"] as? Bool { isDelay = temp }
        if let temp = json["isRepeat"] as? Bool { isRepeat = temp }
        if let temp = json["delayDuration"] as? String { delayDuration = temp }
        if let temp = json["repeatType"] as? String { repeatType = temp }
        if let temp = json["repeatDuration"] as? String { repeatDuration = temp }
        if let temp = json["icon"] as? String { icon = temp }
        if let temp = json["volume"] as? Int { volume = temp }
        return self
    }
}
class PlaylistModel: NSObject, Codable{
    var name = ""
    var mSounds: [SoundModel] = []
    
    init(name: String, mSounds: [SoundModel]) {
        self.name = name
        self.mSounds = mSounds
    }
    override init() {
        
    }
    func initLoad(_ json:  [String: Any]) -> PlaylistModel{
        if let temp = json["name"] as? String { name = temp }
        if let temp = json["mSounds"] as? [[String: Any]] {
            for item in temp{
                var soundAdd = SoundModel()
                soundAdd = soundAdd.initLoad(item)
                mSounds.append(soundAdd)
            }
        }
        return self
    }
}
